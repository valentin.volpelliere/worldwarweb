const TILE_SIZE = 64;

var nbcols = 0;
var nbrows = 0;
var layer = [];
var layerbeneath = [];
var layerover = [];
var layernum = [];
var menuopen = document.querySelector("#buymenu") != null;
var pointUnit = null;


console.log(GRID_INFO);
var lines = GRID_INFO.split('\n')
nbrows = lines.length-1
nbcols = lines[0].split("|").length
lines.forEach(element => {
    var words = element.split('|')
    words.forEach(element2 => {
        var info = element2.split(';')
        var tileinfo = info[0].split(",")
        var unitinfo = (info.length > 1 ? info[1].split(",") : null)

        var textureidtile = tileinfo[0]
        layerbeneath.push(parseInt(textureidtile))
        layer.push( (unitinfo == null )? 0 : parseInt(unitinfo[0]) )
        layerover.push(parseInt(tileinfo[2]))
        if(!(unitinfo == null)){
            if(!(parseInt(unitinfo[1]) == 100))
                layernum.push(Math.floor(46 + (parseInt(unitinfo[1])/10 - 1)))
            else
            layernum.push(0);
        }
        else{
            if(!(parseInt(tileinfo[3])==100))
                layernum.push(Math.floor(46 + (parseInt(tileinfo[3])/10 - 1)))
            else
                layernum.push(0);
        }
    });
});
console.log(layer)

var map = {
    cols: nbcols,
    rows: nbrows,
    tsize: TILE_SIZE,
    layers: [layerbeneath, layer,layerover,layernum],
    getTile: function (layer, col, row) {
        return this.layers[layer][row * map.cols + col];
    }
};

var previous_y = 0
var previous_x = 0

function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var y=getCookie("previous_y");
    if (y != "") {
        previous_y = parseInt(y)
    } else {
        setCookie("previous_y", 0, 1);
    }
    var x=getCookie("previous_x");
    if (x != "") {
        previous_x = parseInt(x)
    } else {
        setCookie("previous_x", 0, 1);
    }
}
checkCookie()

var canvas = document.querySelector("#demo");

var browsersize = {
    width: (window.innerWidth || document.body.clientWidth) * (MENU_OPEN ? 0.8 : 1),
    height: window.innerHeight || document.body.clientHeight
}
var sc_height = Math.min(browsersize.height,map.rows*map.tsize);
var sc_width = Math.min(browsersize.width,map.cols*map.tsize);

canvas.width = sc_width;
canvas.height = sc_height;

canvas.onclick = function(e){
    var xcanvas = canvas.getBoundingClientRect().left
    var ycanvas = canvas.getBoundingClientRect().top
    var point = getPositionOfClickedTile(e.x-xcanvas+Game.camera.x,e.y-ycanvas+Game.camera.y);
    var id_user = parseInt(window.location.href.split("/")[5])
    console.log(point);
    //map.layers[1][point[1]*map.cols+point[0]] = 3;
    //Game._drawMap();
    var id_game = parseInt(window.location.href.split("/")[4])
    if(!menuopen) 
        location.replace("/play/"+ point[0] + "/" + point[1])
    else 
        pointUnit = getPositionOfClickedTile(e.x-xcanvas+Game.camera.x,e.y-ycanvas+Game.camera.y);
}

function sendDataBuyUnit(btn){
	var id_game = parseInt(window.location.href.split("/")[4])
	var id_user = parseInt(window.location.href.split("/")[5])
	
    if(menuopen && pointUnit != null)
        location.replace("/play/"+ pointUnit[0] + "/" + pointUnit[1] + "/" + btn.id.split("|")[1]);
}

function cancelBuy(){
    var id_game = parseInt(window.location.href.split("/")[4])
    var id_user = parseInt(window.location.href.split("/")[5])
    location.replace("/play/-1/-1");
}

function cancelAction(){
    var id_game = parseInt(window.location.href.split("/")[4])
    var id_user = parseInt(window.location.href.split("/")[5])
    location.replace("/play");
}

function endTurn(){
    var id_game = parseInt(window.location.href.split("/")[4])
    var id_user = parseInt(window.location.href.split("/")[5])
    location.replace("/play/pass");
}

function Camera(map, width, height) {
    this.x = previous_x;
    this.y = previous_y;
    this.width = width;
    this.height = height;
    this.maxX = map.cols * map.tsize - width;
    this.maxY = map.rows * map.tsize - height;
    this.x = Math.max(0, Math.min(this.x, this.maxX));
    this.y = Math.max(0, Math.min(this.y, this.maxY));
}

Camera.SPEED = 256*2; // pixels per second

Camera.prototype.move = function (delta, dirx, diry) {
    // move camera
    this.x += dirx * Camera.SPEED * delta;
    this.y += diry * Camera.SPEED * delta;
    // clamp values
    this.x = Math.max(0, Math.min(this.x, this.maxX));
    this.y = Math.max(0, Math.min(this.y, this.maxY));
};

Game.load = function () {
    return [
        Loader.loadImage('tiles', '/assets/tiles64ex.png'),
    ];
};

Game.init = function () {
    Keyboard.listenForEvents(
        [Keyboard.LEFT, Keyboard.RIGHT, Keyboard.UP, Keyboard.DOWN]);
    this.tileAtlas = Loader.getImage('tiles');
    this.camera = new Camera(map, sc_width,sc_height);

    // create a canvas for each layer
    this.layerCanvas = map.layers.map(function () {
        var c = document.createElement('canvas');
        c.width = sc_width;
        c.height = sc_height;
        return c;
    });

    // initial draw of the map
    this._drawMap();
};

Game.update = function (delta) {
    this.hasScrolled = false;
    // handle camera movement with arrow keys
    var dirx = 0;
    var diry = 0;
    if (Keyboard.isDown(Keyboard.LEFT)) { dirx = -1; }
    if (Keyboard.isDown(Keyboard.RIGHT)) { dirx = 1; }
    if (Keyboard.isDown(Keyboard.UP)) { diry = -1; }
    if (Keyboard.isDown(Keyboard.DOWN)) { diry = 1; }

    if (dirx !== 0 || diry !== 0) {
        this.camera.move(delta, dirx, diry);
        this.hasScrolled = true;
    }
};

Game._drawMap = function () {
    setCookie("previous_x", this.camera.x, 1);
    setCookie("previous_y", this.camera.y, 1);
    map.layers.forEach(function (layer, index) {
        this._drawLayer(index);
    }.bind(this));
};

Game._drawLayer = function (layer) {
    var context = this.layerCanvas[layer].getContext('2d');
    context.clearRect(0, 0, sc_width, sc_height);

    var startCol = Math.floor(this.camera.x / map.tsize);
    var endCol = startCol + (this.camera.width / map.tsize);
    var startRow = Math.floor(this.camera.y / map.tsize);
    var endRow = startRow + (this.camera.height / map.tsize);
    var offsetX = -this.camera.x + startCol * map.tsize;
    var offsetY = -this.camera.y + startRow * map.tsize;

    for (var c = startCol; c <= endCol+1; c++) {
        for (var r = startRow; r <= endRow+1; r++) {
            var tile = map.getTile(layer, c, r);
            var x = (c - startCol) * map.tsize + offsetX;
            var y = (r - startRow) * map.tsize + offsetY;
            if (tile !== 0) { // 0 => empty tile
                context.drawImage(
                    this.tileAtlas, // image
                    (tile - 1) * map.tsize, // source x
                    0, // source y
                    map.tsize, // source width
                    map.tsize, // source height
                    Math.round(x),  // target x
                    Math.round(y), // target y
                    map.tsize, // target width
                    map.tsize // target height
                );
            }
        }
    }
};

Game.render = function () {
    // re-draw map if there has been scroll
    if (this.hasScrolled) {
        this._drawMap();
    }

    // draw the map layers into game context
    for(var i=0;i<map.layers.length;i++){
        this.ctx.drawImage(this.layerCanvas[i], 0, 0);
    }
};


//////////////////////////


function getPositionOfClickedTile(x,y){
    var selx = Math.floor(x/TILE_SIZE);
    var sely = Math.floor(y/TILE_SIZE);
    return [selx,sely];
}