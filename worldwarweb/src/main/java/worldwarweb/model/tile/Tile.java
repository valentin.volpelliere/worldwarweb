package worldwarweb.model.tile;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import worldwarweb.model.Game;
import worldwarweb.model.unit.Unit;

@Entity
@Table(name="tile")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Tile {
	
	@Id @Column
	@GeneratedValue(generator = "seqTileAndUnit")
	@SequenceGenerator(name = "seqTileAndUnit", sequenceName = "seq_tile_and_unit" ,allocationSize=1 )
	private Long id;
	
	@Column(length=60,nullable=false)
	private String name;
	@Column(length=60,nullable=false)
	private int textureid = 0;
	@Column(length=40,nullable=false)
	private int x,y;
	@Column(nullable = false)
	private boolean selected = false;
	@Column(nullable = false)
	private int select_tex_id = 0;
	
	@ManyToOne
	private Game game;
	
	public String toDataHTML () {
		return "" + textureid + ",NULL" + "," + select_tex_id + "," + (this instanceof Building ? 100*((Building)this).getHp()/((Building)this).getHpmax() : 100);
	}
	
	abstract public float getSpeedCoef (Unit unit);
	abstract public float getDamageCoef (Unit unit);
	abstract public float getRangeObstructCoef (Unit unit);
	abstract public float getRangeBonusCoef (Unit unit);
	abstract public float getDefenseCoef (Unit unit);
	
	
	public Tile(String name, int textureid, int x, int y) {
		this.name = name;
		this.textureid = textureid;
		this.x = x;
		this.y = y;
	}
	
	public Tile() {
		super();
	}
	
	@Override
	public String toString() {
		return "Tile [hashcode =" + System.identityHashCode(this) + ", name=" + name + ", textureid=" + textureid + ", x=" + x + ", y=" + y + "]\n";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getTextureid() {
		return textureid;
	}
	public void setTextureid(int textureid) {
		this.textureid = textureid;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	
	
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public int getSelect_tex_id() {
		return select_tex_id;
	}
	public void setSelect_tex_id(int select_tex_id) {
		this.select_tex_id = select_tex_id;
		this.selected = (select_tex_id != 0);
	}
	
	public ArrayList<Unit> getAllUnitGoodSpeed (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getSpeedCoef(unit) > 1)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitBadSpeed (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getSpeedCoef(unit) < 1 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitGoodDamage (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getDamageCoef(unit) > 1 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitBadDamage (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getDamageCoef(unit) < 1 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitGoodRangeObstruct (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getRangeObstructCoef(unit) < 1 && getRangeObstructCoef(unit) > 0 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitGoodBonusCoef (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getRangeBonusCoef(unit) > 1 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitGoodDefence (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getDefenseCoef(unit) > 1 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
	public ArrayList<Unit> getAllUnitBadDefence (ArrayList<Unit> all){
		ArrayList<Unit> arr = new ArrayList<>();
		for(Unit unit : all) {
			if(getDefenseCoef(unit) < 1 && getDefenseCoef(unit) > 0 && getSpeedCoef(unit) > 0)
				arr.add(unit);
		}
		return arr;
	}
}
