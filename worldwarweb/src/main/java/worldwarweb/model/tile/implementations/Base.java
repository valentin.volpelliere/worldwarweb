package worldwarweb.model.tile.implementations;


import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;

@Entity
public class Base extends Building{

	public Base(int x, int y, Player ownerB) {
		super("base", 132, x, y, null, ownerB, 150, 1200, 0);
		System.out.println(getOwner());
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid()+1);
				break;
			case "BLUE":
				setTextureid(getTextureid()+2);
				break;
			case "YELLOW":
				setTextureid(getTextureid()+3);
				break;
			case "GREEN":
				setTextureid(getTextureid()+4);
				break;
			}
		}
	}
	
	public Base() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(isOwner(unit.getOwner()))
			return 1;
		else 
			return 0;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}
}
