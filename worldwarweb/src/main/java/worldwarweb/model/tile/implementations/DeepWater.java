package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.Submarine;

@Entity
public class DeepWater extends Tile {
	
	public DeepWater() {}
	
	public DeepWater(int x, int y) {
		super("deep water", 10, x, y);
	}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitGround || (unit instanceof Vehicle && !(unit instanceof Barge)))
			return 0;
		if(unit instanceof UnitAir)
			return 1;
		return 1.25f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		if(unit instanceof Submarine)
			return 1.75f;
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		return 1.25f;
	}

}
