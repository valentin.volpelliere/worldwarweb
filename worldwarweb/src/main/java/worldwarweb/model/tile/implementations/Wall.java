package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;

@Entity
public class Wall extends Tile{

	public Wall(int x, int y) {
		super("wall", 4, x, y);
	}
	
	public Wall() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater)
			return 0;
		return 0;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 2;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		return 1;
	}

}
