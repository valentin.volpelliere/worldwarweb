package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;

@Entity
public class Beach extends Tile{
	
	public Beach() {}
	
	public Beach(int x, int y) {
		super("beach", 14, x, y);
	}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater)
			return 0.5f;
		return 0.75f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		if(unit instanceof BattleShip)
			return 1.5f;
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		if(unit instanceof UnitWater)
			return 0.75f;
		return 1;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		if(unit instanceof UnitWater)
			return 1.5f;
		return 1;
	}

}
