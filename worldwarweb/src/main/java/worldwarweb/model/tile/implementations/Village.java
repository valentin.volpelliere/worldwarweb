package worldwarweb.model.tile.implementations;

import java.util.ArrayList;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.implementations.Soldier;

@Entity
public class Village extends Building{

	public Village(int x, int y,Player owner) {
		super("Village", 122, x, y, null, owner, 100, 600, 0);
		if(getOwner() !=null && getOwner().getColor()!=null) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid()+1);
				break;
			case "BLUE":
				setTextureid(getTextureid()+2);
				break;
			case "YELLOW":
				setTextureid(getTextureid()+3);
				break;
			case "GREEN":
				setTextureid(getTextureid()+4);
				break;
			}
		}
		setProductible(null);
	}
	
	public Village() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 2;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		return 1;
	}
	
}
