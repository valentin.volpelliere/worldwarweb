package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.Boat;

@Entity
public class StillWater extends Tile {

	public StillWater(int x, int y) {
		super("still water", 5, x, y);
	}
	
	public StillWater() {}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitGround)
			return 0;
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater || (unit instanceof Barge))
			return 1.25f;
		return 0;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		if(unit instanceof Boat)
			return 1.25f;
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
