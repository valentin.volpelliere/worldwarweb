package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Archer;
import worldwarweb.model.unit.implementations.Barge;

@Entity
public class Mountain extends Tile{

	public Mountain(int x, int y) {
		super("mountain", 3, x, y);
	}
	
	public Mountain() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof Vehicle || unit instanceof UnitWater || (unit instanceof Barge))
			return 0;
		return 0.25f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		//TODO ADJUST
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 2;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 0.5f;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		if(unit instanceof Archer)
			return 3;
		return 1.5f;
	}
}
