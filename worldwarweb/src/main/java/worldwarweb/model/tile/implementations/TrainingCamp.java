package worldwarweb.model.tile.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import javassist.expr.NewArray;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.implementations.Archer;
import worldwarweb.model.unit.implementations.ArmoredVehicle;
import worldwarweb.model.unit.implementations.Car;
import worldwarweb.model.unit.implementations.Destroyer;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.model.unit.implementations.Tank;

@Entity
public class TrainingCamp extends Building {
	public TrainingCamp(int x, int y, Player ownerB) {
		super("training camp", 127, x, y, new ArrayList<>(), ownerB, 0, 800, 0);
		if(getOwner() !=null && getOwner().getColor()!=null) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid()+1);
				break;
			case "BLUE":
				setTextureid(getTextureid()+2);
				break;
			case "YELLOW":
				setTextureid(getTextureid()+3);
				break;
			case "GREEN":
				setTextureid(getTextureid()+4);
				break;
			}
		}
		Soldier soldier = new Soldier(null, x-1, y);
		soldier.setBuildingProd(this);
		Archer archer = new Archer(null, x-1, y);
		archer.setBuildingProd(this);
		Tank tank = new Tank(null, x-1, y);
		tank.setBuildingProd(this);
		Car car = new Car(null, x-1, y);
		car.setBuildingProd(this);
		ArmoredVehicle armoredVehicle = new ArmoredVehicle(null, x-1, y);
		armoredVehicle.setBuildingProd(this);
		Destroyer destroyer = new Destroyer(null, x-1, y);
		destroyer.setBuildingProd(this);
		getProductible().add(tank);
		getProductible().add(archer);
		getProductible().add(soldier);
		getProductible().add(car);
		getProductible().add(armoredVehicle);
		getProductible().add(destroyer);
	}
	
	public TrainingCamp() {}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}
}
