package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;

@Entity
public class CactusFields extends Tile {
	
	public CactusFields() {}
	
	public CactusFields(int x, int y) {
		super("cactus fields", 13, x, y);
	}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof Vehicle || unit instanceof UnitWater || unit instanceof Barge)
			return 0;
		return 0.5f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 2f;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1.25f;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
