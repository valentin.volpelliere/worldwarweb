package worldwarweb.model.tile.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Submarine;

@Entity
public class Port extends Building {

	public Port(int x, int y, Player ownerB) {
		super("port", 137, x, y, new ArrayList<>(), ownerB, 0, 1000, 0);
		if(getOwner() !=null && getOwner().getColor()!=null) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid()+1);
				break;
			case "BLUE":
				setTextureid(getTextureid()+2);
				break;
			case "YELLOW":
				setTextureid(getTextureid()+3);
				break;
			case "GREEN":
				setTextureid(getTextureid()+4);
				break;
			}
		}
		Boat boat = new Boat(null,0,0);
		boat.setBuildingProd(this);
		getProductible().add(boat);
		Submarine submarine = new Submarine(null,0,0);
		submarine.setBuildingProd(this);
		getProductible().add(submarine);
		Barge barge = new Barge(null,0,0);
		barge.setBuildingProd(this);
		getProductible().add(barge);
		BattleShip battleShip = new BattleShip(null,0,0);
		battleShip.setBuildingProd(this);
		getProductible().add(battleShip);
	}
	
	public Port() {}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
