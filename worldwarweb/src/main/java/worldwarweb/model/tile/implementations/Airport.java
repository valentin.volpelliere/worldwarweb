package worldwarweb.model.tile.implementations;

import java.util.ArrayList;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Helicopter;
import worldwarweb.model.unit.implementations.Plane;
import worldwarweb.model.unit.implementations.StealthPlane;
import worldwarweb.model.unit.implementations.Submarine;

@Entity
public class Airport extends Building{
	public Airport(int x, int y, Player ownerB) {
		super("airport", 142, x, y, new ArrayList<>(), ownerB, 0, 1000, 0);
		if(getOwner() !=null && getOwner().getColor()!=null) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid()+1);
				break;
			case "BLUE":
				setTextureid(getTextureid()+2);
				break;
			case "YELLOW":
				setTextureid(getTextureid()+3);
				break;
			case "GREEN":
				setTextureid(getTextureid()+4);
				break;
			}
		}
		Plane plane = new Plane(null,0,0);
		plane.setBuildingProd(this);
		getProductible().add(plane);
		Helicopter helicopter = new Helicopter(null,0,0);
		helicopter.setBuildingProd(this);
		getProductible().add(helicopter);
		StealthPlane stealthPlane = new StealthPlane(null,0,0);
		stealthPlane.setBuildingProd(this);
		getProductible().add(stealthPlane);

	}
	
	public Airport() {}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}
}
