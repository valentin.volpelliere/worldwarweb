package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.Commander;
import worldwarweb.model.unit.implementations.Soldier;

@Entity
public class Forest extends Tile{

	public Forest(int x, int y) {
		super("forest",2, x, y);
	}
	
	public Forest() {super();}
	
	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater || unit instanceof Barge)
			return 0;
		if(unit instanceof Vehicle)
			return 0.25f;
		return 0.5f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		if(unit instanceof Soldier || unit instanceof Commander)
			return 1.5f;
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 2;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		return 0.75f;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		return 0.75f;
	}

}
