package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Building;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;

@Entity
public class Gate extends Building{

	public Gate(int x, int y) {
		super("gate", 7, x, y, null, null, 0, 100, 0);
	}
	
	public Gate() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater || (unit instanceof Barge))
			return 0;
		return 0;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		return 0;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 0;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
