package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.Soldier;

@Entity
public class Grass extends Tile{

	public Grass(int x, int y) {
		super("grass",1, x, y);
	}
	
	public Grass() {super();}

	@Override
	public float getSpeedCoef(Unit unit) {
		// TODO Auto-generated method stub
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater || (unit instanceof Barge))
			return 0;
		if(unit instanceof Soldier)
			return 1.25f;
		return 1;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
