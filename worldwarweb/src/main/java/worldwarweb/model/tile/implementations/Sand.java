package worldwarweb.model.tile.implementations;

import javax.persistence.Entity;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.Tank;

@Entity
public class Sand extends Tile {

	public Sand(int x, int y) {
		super("sand", 11, x, y);
	}
	
	public Sand() {}

	@Override
	public float getSpeedCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		if(unit instanceof UnitWater || ((unit instanceof Barge)))
			return 0;
		if(unit instanceof Vehicle)
			return 0;
		return 0.75f;
	}

	@Override
	public float getDamageCoef(Unit unit) {
		if(unit instanceof Tank)
			return 1.5f;
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		if(unit instanceof UnitAir)
			return 1;
		return 0.75f;
	}

	@Override
	public float getRangeObstructCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public float getRangeBonusCoef(Unit unit) {
		// TODO Auto-generated method stub
		return 1;
	}

}
