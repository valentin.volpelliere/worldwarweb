package worldwarweb.model.tile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Commander;

@Entity
@Table(name="building")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Building extends Tile{
	@Column(length=40,nullable=false)
	private int money_prod;
	@Column(length=40,nullable=false)
	private int hp;
	@Column(length=40,nullable=false)
	private int hpmax;
	@Column(length=40,nullable=false)
	private int cooldown;
	
	@OneToMany(mappedBy="buildingProd",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Unit> productible;
	
	@ManyToOne
	private Player owner;
	
	public Building(String name, int textureid, int x, int y, List<Unit> productible, Player ownerB, int money_prod,int hp, int cooldown) {
		super(name, textureid, x, y);
		this.productible = productible;
		this.owner = ownerB;
		this.money_prod = money_prod;
		this.hp = hp;
		this.hpmax = hp;
		this.cooldown = cooldown;
	}
	
	public Building() {
		super();
	}
	
	public boolean canAttackTile(Tile b) {
		return Math.abs(b.getX() - getX()) <= 1 && Math.abs(b.getY() - getY()) <= 1;
	}
	
	public int calculateAttackDamage(Unit enemy) {
		Tile enepos = enemy.getTilePosition();
		float damage = (30 * (1 - enepos.getDefenseCoef(enemy) ) ) * (hp*1f/hpmax);
		return (int)Math.max(damage,1);
	}
	
	public void counterAttack(Unit enemy) {
		if(canAttackTile(enemy.getTilePosition())) {
			System.out.println("counter-attack : " + this + " --> " + enemy);
			float coef_counteratk = 0.8f;
			int newhp = Math.max(0,enemy.getHp() - (int)(coef_counteratk*calculateAttackDamage(enemy)));
			enemy.setHp(newhp);
			if(enemy.getHp() == 0)
			{
				if(enemy instanceof Commander)
				{
					Game game = this.getOwner().getGame();
					Player player = enemy.getOwner();
					player.setDead(true);
					player.getUnits().clear();
					for(Building currentBuilding : player.getBuilding())
					{
						currentBuilding.setOwner(null);
					}
					ArrayList<Player> players_alive = new ArrayList<Player>();
					for(Player currentPlayer : game.getPlayers())
					{
						if(currentPlayer.isDead() == false)
						{
							players_alive.add(currentPlayer);
						}
					}
					if(players_alive.size() == 1)
					{
						game.setWinner(players_alive.get(0));
						game.setEnd_time(LocalDateTime.now());
						return;
					}
				}
				if(enemy instanceof Vehicle)
					((Vehicle)enemy).cEstBallotVousDecedez();
				enemy.getOwner().getUnits().remove(enemy);
			}
				
		}
	}
	
	public void updateTexId() {
		int texid = getTextureid();
		int basetex = 122;
		while(texid >= basetex)
			basetex+=5;
		basetex -= 5;
		if(getOwner() == null) 
			setTextureid(basetex);
		else {
			if(getOwner().getColor().equals("Red"))
				setTextureid(basetex+1);
			if(getOwner().getColor().equals("Blue"))
				setTextureid(basetex+2);
			if(getOwner().getColor().equals("Yellow"))
				setTextureid(basetex+3);
			if(getOwner().getColor().equals("Green"))
				setTextureid(basetex+4);
		}
	}
	
	public void buyUnit (int option,Tile tile,Long nextId) {
		if(productible != null && productible.size() > 0 && productible.size() > option && option >= 0) {
			int x1 = getX();
			int x2 = tile.getX();
			int y1 = getY();
			int y2 = tile.getY();
			Unit toBuy = productible.get(option);
			if(toBuy.getCost() <=  getOwner().getMoney()) {
				boolean north = x2 == x1 && y2 == y1-1,
						south = x2 == x1 && y2 == y1+1,
						east = x2 == x1+1 && y2 == y1,
						west = x2 == x1-1 && y2 == y1;
				if(north || south || east || west) {
					getOwner().setMoney(getOwner().getMoney() - toBuy.getCost());
					Unit newunit;
					try {
						newunit = (Unit) toBuy.clone();
						newunit.setOwner(getOwner());
						newunit.setBuildingProd(null);
						newunit.setId(nextId);
						newunit.setX(tile.getX());
						newunit.setY(tile.getY());
						newunit.setTextureid(newunit.getTextureid() + (getOwner().getColor().equals("Red") ? 0 : 26));
						getOwner().getUnits().add(newunit);
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	@Override
	public String toString() {
		return "Building [productible=" + productible + ", owner=" + owner + ", money_prod=" + money_prod + ", hp=" + hp
				+ ", cooldown=" + cooldown + "]" + super.toString();
	}
	
	public boolean isOwner(Player p) {
		if(this.getOwner() != null && p.getColor().equals(this.getOwner().getColor()))
			return true;
		return false;
	}
	
	public void heal(int points)
	{
		this.hp += points;
		if(this.hp>this.hpmax) this.hp=this.hpmax;
	}
	
	@Override
	public float getSpeedCoef(Unit unit) {
		if(isOwner(unit.getOwner()))
			return 1;
		else 
			return 0;
	}
	
	@Override
	public float getDamageCoef(Unit unit) {
		return 1;
	}

	@Override
	public float getDefenseCoef(Unit unit) {
		return 1;
	}
	
	public int generateMoney() {
		return (int)(money_prod*(hp*1f/hpmax));
	}
	
	public List<Unit> getProductible() {
		return productible;
	}

	public void setProductible(List<Unit> productible) {
		this.productible = productible;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public int getMoney_prod() {
		return money_prod;
	}

	public void setMoney_prod(int money_prod) {
		this.money_prod = money_prod;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public int getHpmax() {
		return hpmax;
	}

	public void setHpmax(int hpmax) {
		this.hpmax = hpmax;
	}
	
	
	
}
