package worldwarweb.model.unit;

import java.util.List;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.Base;
import worldwarweb.model.tile.implementations.Wall;
import worldwarweb.model.unit.implementations.Commander;
import worldwarweb.service.GameService;


@Entity
@Table(name="unit")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Unit implements Cloneable{

	@Id @Column
	@GeneratedValue(generator = "seqTileAndUnit")
	@SequenceGenerator(name = "seqTileAndUnit", sequenceName = "seq_tile_and_unit" ,allocationSize=1 )
	private Long id;
	
	@Column(length=60,nullable=false)
	private String name;
	@Column(length=60,nullable=false)
	private int textureid;
	
	@Column(length=40,nullable=false)
	private int x,y;
	
	@Column(length=40,nullable=false)
	private int range_mvmt_max;
	@Column(length=40,nullable=false)
	private int range_mvmt;
	@Column(length=40,nullable=false)
	private int range_atk;
	@Column(length=40,nullable=false)
	private int hp;
	@Column(length=40,nullable=false)
	private int hpmax;
	@Column(length=40,nullable=false)
	private int atk;
	@Column(length=40,nullable=false)
	private int cost;
	@Column(length=40,nullable=false)
	private int max_attack_per_round;
	@Column(length=40,nullable=false)
	private int nb_attack_round;
	
	@ManyToOne
	private Player owner;
	@ManyToOne
	private Vehicle passenger;
	
	@ManyToOne
	private Building buildingProd;
	
	public Unit()
	{
		super();
	}

	public Unit(Player owner, String name, int textureid, int x,int y, int range_mvmt, int range_atk, int hp,int atk,int cost, int max_attack_per_round) {
		super();
		this.owner = owner;
		this.passenger = null;
		this.name = name;
		this.textureid = textureid;
		this.range_mvmt_max = range_mvmt;
		this.x = x;
		this.y=y;
		this.range_mvmt = range_mvmt;
		this.range_atk = range_atk;
		this.hp = hp;
		this.max_attack_per_round = max_attack_per_round;
		this.nb_attack_round = 0;
		this.hpmax = hp;
		this.atk = atk;
		this.cost=cost;
	}
	
	public String toDataHTML() {
		return "" + textureid + "," + (hp*100/hpmax);
	}
	
	public int getRange_mvmt_max() {
		return range_mvmt_max;
	}

	public void setRange_mvmt_max(int range_mvmt_max) {
		this.range_mvmt_max = range_mvmt_max;
	}
	
	public int getMax_attack_per_round() {
		return max_attack_per_round;
	}

	public void setMax_attack_per_round(int max_attack_per_round) {
		this.max_attack_per_round = max_attack_per_round;
	}

	public int getNb_attack_round() {
		return nb_attack_round;
	}

	public void setNb_attack_round(int nb_attack_round) {
		this.nb_attack_round = nb_attack_round;
	}
	
	public void pafFauteuilleRoulant(){
		this.setRange_mvmt(0);
	}
	
	public boolean isVehicle() {
		return this instanceof Vehicle;
	}
	
	public List<Unit> getPassengerList(){
		if(!(this instanceof Vehicle))
			return null;
		return ((Vehicle)this).getUnits();
	}

	public abstract float getAttackEfficiency (Unit enemy);
	
	public ArrayList<Tile> getAccessibleTiles() {
		Game game = this.getOwner().getGame();
		ArrayList<Tile> accessibleTiles = new ArrayList<Tile>();
		GameService.spreadSpeedAlgo(game,this,accessibleTiles, this.getX(),this.getY(),this.getRange_mvmt());
		
		return accessibleTiles;
	}
	
	public void getAttackableTiles(ArrayList<Tile> attackableTiles, int x, int y) {
		Game game = this.getOwner().getGame();
		GameService.spreadAttackAlgo(game,this,attackableTiles, x,y,this.getRange_atk()*game.getTileAt(x, y).getRangeBonusCoef(this));
	}
	
	public Tile getTilePosition () {
		return this.getOwner().getGame().getTileAt(x, y);
	}

	public ArrayList<Tile> getAllAttackableTiles(ArrayList<Tile> accessibleTiles) {
		ArrayList<Tile> attackableTiles = new ArrayList<Tile>();
		Game game = this.getOwner().getGame();
		for(Tile t : accessibleTiles) {
			if( game != null && game.getTileAt(x, y) != null) {
				if(this.getRange_atk()*game.getTileAt(x, y).getRangeBonusCoef(this) == 1) {
					int x = t.getX();
					int y = t.getY();
					Tile tile  = game.getTileAt( x   , y   );
					Tile up    = game.getTileAt( x	 , y-1 );
					Tile down  = game.getTileAt( x	 , y+1 );
					Tile left  = game.getTileAt( x-1 , y   );
					Tile right = game.getTileAt( x+1 , y   );
					
					if(!attackableTiles.contains(tile) && !(tile instanceof Wall))
						attackableTiles.add(tile);
					
					if(up != null && up.getRangeObstructCoef(this)>=0. && !attackableTiles.contains(up) && !(up instanceof Wall))
						attackableTiles.add(up);
					if(right != null && right.getRangeObstructCoef(this)>=0. && !attackableTiles.contains(right) && !(right instanceof Wall))
						attackableTiles.add(right);
					if(down  != null && down. getRangeObstructCoef(this)>=0. && !attackableTiles.contains(down) && !(down instanceof Wall))
						attackableTiles.add(down);
					if(left  != null && left. getRangeObstructCoef(this)>=0. && !attackableTiles.contains(left) && !(left instanceof Wall))
						attackableTiles.add(left );
					
				}
				else
					getAttackableTiles(attackableTiles, t.getX(), t.getY());
			}
		}
		return attackableTiles;
	}
	
	public void heal(int points)
	{
		this.hp += points;
		if(this.hp>this.hpmax) this.hp=this.hpmax;
	}
	
	public boolean canAttackTile(Tile t) {
		return getAllAttackableTiles(getAccessibleTiles()).contains(t);
	}
	
	public void setTileSelectionBox() {
		Game game = getOwner().getGame();
		
		ArrayList<Tile> accessibleTiles = getAccessibleTiles();
		ArrayList<Tile> attackableTiles = null;
		if(this.getAtk() > 0)
			attackableTiles = getAllAttackableTiles(accessibleTiles);
		
		//Visualize attackable range for each cases
		if(attackableTiles != null)
		{
			for(Tile t : attackableTiles) {
				game.getTileAt(t.getX(), t.getY()).setSelect_tex_id(GameService.SELECTED_TILE_RED);
			}
		}
		
		//Visualize movement range
		for(Tile t : accessibleTiles) {
			game.getTileAt(t.getX(), t.getY()).setSelect_tex_id(GameService.SELECTED_TILE_ORANGE);
		}
	}
	
	public int calculateAttackDamage(Unit enemy) {
		Tile pos = getTilePosition();
		Tile enepos = enemy.getTilePosition();
		float damage = (atk * (getAttackEfficiency(enemy) + pos.getDamageCoef(this) - enepos.getDefenseCoef(enemy) ) ) * (hp*1f/hpmax);
		return (int)Math.max(damage,1);
	}
	
	public void attack(Unit enemy) {
		if(canAttackTile(enemy.getTilePosition())) {
			System.out.println("attack : " + this + " --> " + enemy);
			int newhp = Math.max(0,enemy.getHp() - calculateAttackDamage(enemy));
			enemy.setHp(newhp);
			if(enemy.getHp() == 0)
			{
				if(enemy instanceof Commander)
				{
					Game game = this.getOwner().getGame();
					Player player = enemy.getOwner();
					player.setDead(true);
					player.getUnits().clear();
					for(Building currentBuilding : player.getBuilding())
					{
						currentBuilding.setOwner(null);
					}
					ArrayList<Player> players_alive = new ArrayList<Player>();
					for(Player currentPlayer : game.getPlayers())
					{
						if(currentPlayer.isDead() == false)
						{
							players_alive.add(currentPlayer);
						}
					}
					if(players_alive.size() == 1)
					{
						game.setWinner(players_alive.get(0));
						game.setEnd_time(LocalDateTime.now());
						return;
					}
				}
				if(enemy instanceof Vehicle)
					((Vehicle)enemy).cEstBallotVousDecedez();
				enemy.getOwner().getUnits().remove(enemy);
			}
			else
				enemy.counterAttack(this);
		}
	}
	
	public void counterAttack(Unit enemy) {
		if(canAttackTile(enemy.getTilePosition())) {
			System.out.println("counter-attack : " + this + " --> " + enemy);
			ArrayList<Tile> attackableTiles = new ArrayList<Tile>();
			this.getAttackableTiles(attackableTiles, this.getX(), this.getY());
			if(attackableTiles.contains(enemy.getOwner().getGame().getTileAt(enemy.getX(), enemy.getY())))
			{
				float coef_counteratk = 0.8f;
				int newhp = Math.max(0,enemy.getHp() - (int)(coef_counteratk*calculateAttackDamage(enemy)));
				enemy.setHp(newhp);
			}
			if(enemy.getHp() == 0)
			{
				if(enemy instanceof Commander)
				{
					Game game = this.getOwner().getGame();
					Player player = enemy.getOwner();
					player.setDead(true);
					player.getUnits().clear();
					for(Building currentBuilding : player.getBuilding())
					{
						currentBuilding.setOwner(null);
					}
					ArrayList<Player> players_alive = new ArrayList<Player>();
					for(Player currentPlayer : game.getPlayers())
					{
						if(currentPlayer.isDead() == false)
						{
							players_alive.add(currentPlayer);
						}
					}
					if(players_alive.size() == 1)
					{
						game.setWinner(players_alive.get(0));
						game.setEnd_time(LocalDateTime.now());
						return;
					}
				}
				if(enemy instanceof Vehicle)
					((Vehicle)enemy).cEstBallotVousDecedez();
				enemy.getOwner().getUnits().remove(enemy);
			}
				
		}
	}
	
	public int calculateAttackDamage(Building enemy) {
		Tile pos = getTilePosition();
		float damage = (atk * (pos.getDamageCoef(this) ) ) * (hp*1f/hpmax);
		return (int)Math.max(damage,1);
	}
	
	public boolean attack(Building enemy) {
		Boolean north = enemy.getX() == getX() && enemy.getY() == getY()-1,
			south  = enemy.getX() == getX() && enemy.getY() == getY()+1,
			east   = enemy.getX() == getX()-1 && enemy.getY() == getY(),
			west   = enemy.getX() == getX()+1 && enemy.getY() == getY();
		if(north || south || east || west) {
			System.out.println("attack : " + this + " --> " + enemy);
			if(enemy.getOwner() != null) {
				int newhp = Math.max(0,enemy.getHp() - calculateAttackDamage(enemy));
				enemy.setHp(newhp);
				if(enemy.getHp() == 0) {
					if(enemy instanceof Base)
					{
						Game game = this.getOwner().getGame();
						Player player = enemy.getOwner();
						player.setDead(true);
						player.getUnits().clear();
						for(Building currentBuilding : player.getBuilding())
						{
							currentBuilding.setOwner(null);
						}
						ArrayList<Player> players_alive = new ArrayList<Player>();
						for(Player currentPlayer : game.getPlayers())
						{
							if(currentPlayer.isDead() == false)
							{
								players_alive.add(currentPlayer);
							}
						}
						if(players_alive.size() == 1)
						{
							game.setWinner(players_alive.get(0));
							game.setEnd_time(LocalDateTime.now());
							return true;
						}
					}
					enemy.setOwner(null);
					enemy.updateTexId();
				}
				else
					enemy.counterAttack(this);
				return true;
			}
			else {
				enemy.setOwner(getOwner());
				enemy.setHp((int)(enemy.getHpmax()/2*1.*(this.getHp()/this.getHpmax())));
				getOwner().getBuilding().add(enemy);
				enemy.updateTexId();
				return true;
			}
		}
		return false;
	}
	
	public Object clone() throws CloneNotSupportedException{
		return super.clone(); 
	}
	
	@Override
	public String toString() {
		return "Unit [name=" + name + 
				", textureid="+textureid+
				", x=" + x +
				", y=" + y +
				", range_mvmt=" + range_mvmt + 
				", range_atk=" + range_atk + 
				", hp=" + hp + 
				", atk=" + atk + 
		"]";
	}

	public Player getOwner() {
		return owner;
	}


	public void setOwner(Player owner) {
		this.owner = owner;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getTextureid() {
		return textureid;
	}

	public void setTextureid(int textureid) {
		this.textureid = textureid;
	}

	public int getHpmax() {
		return hpmax;
	}

	public void setHpmax(int hpmax) {
		this.hpmax = hpmax;
	}

	public Building getBuildingProd() {
		return buildingProd;
	}

	public void setBuildingProd(Building buildingProd) {
		this.buildingProd = buildingProd;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getRange_mvmt() {
		return range_mvmt;
	}


	public void setRange_mvmt(int range_mvmt) {
		this.range_mvmt = range_mvmt;
	}


	public int getRange_atk() {
		return range_atk;
	}


	public void setRange_atk(int range_atk) {
		this.range_atk = range_atk;
	}


	public int getHp() {
		return hp;
	}


	public void setHp(int hp) {
		this.hp = hp;
	}


	public int getAtk() {
		return atk;
	}


	public void setAtk(int atk) {
		this.atk = atk;
	}

	public Vehicle getPassenger() {
		return passenger;
	}

	public void setPassenger(Vehicle passenger) {
		this.passenger = passenger;
	}
	
}
