package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;

@Entity
public class Barge extends Vehicle{
	public Barge()
	{
		super();
	}
	public Barge(Player owner,int x,int y) {
		super(owner, "barge", 80,x,y, 6, 0, 160, 0,150,0,2);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
}
