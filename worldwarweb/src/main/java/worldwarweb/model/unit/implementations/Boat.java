package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;

@Entity
public class Boat extends UnitWater{
	public Boat()
	{
		super();
	}
	public Boat(Player owner,int x,int y) {
		super(owner, "boat", 79,x,y, 5, 2, 300, 120,250,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 0;
		if(enemy instanceof Archer || enemy instanceof Soldier || enemy instanceof Car || enemy instanceof Commander)
			return 1.25f;
		return super.getAttackEfficiency(enemy);
	}
}
