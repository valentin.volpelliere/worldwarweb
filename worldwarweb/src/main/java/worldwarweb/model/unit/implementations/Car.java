package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.Vehicle;

@Entity
public class Car extends Vehicle{
	public Car()
	{
		super();
	}
	public Car(Player owner,int x,int y) {
		super(owner, "car", 61,x,y, 6, 0, 200, 0,300,0,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 

}
