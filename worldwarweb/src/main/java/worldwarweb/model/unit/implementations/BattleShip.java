package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;

@Entity
public class BattleShip extends UnitWater{
	public BattleShip()
	{
		super();
	}
	public BattleShip(Player owner,int x,int y) {
		super(owner, "battleship", 78,x,y, 5, 3, 700, 320,1300,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 1.5f;
		if(enemy instanceof Destroyer || enemy instanceof StealthPlane)
			return 1.25f;
		return 1;
	}
}
