package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;

@Entity
public class Soldier extends UnitGround{
	public Soldier()
	{
		super();
	}
	
	public Soldier(Player owner,int x,int y) {
		super(owner, "Soldier", 59,x,y, 3, 1, 200, 80,100,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 0;
		if(enemy instanceof Archer)
			return 1.25f;
		if(enemy instanceof Commander)
			return 1.1f;
		return 1;
	}
}
