package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;

@Entity
public class StealthPlane extends UnitAir{
	public StealthPlane()
	{
		super();
	}
	public StealthPlane(Player owner,int x,int y) {
		super(owner, "stealthPlane", 76,x,y, 6, 1, 300, 190,1100,2);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 0;
		return 1.1f;
	}
}
