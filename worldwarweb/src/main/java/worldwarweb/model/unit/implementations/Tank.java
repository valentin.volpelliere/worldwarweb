package worldwarweb.model.unit.implementations;
import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;

@Entity
public class Tank extends UnitGround{
	public Tank(){
		super();
	}
	
	public Tank(Player owner,int x,int y) {
		super(owner, "tank", 62,x,y, 4, 2, 330, 110,400,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 0;
		if(enemy instanceof Archer || enemy instanceof Soldier || enemy instanceof Car)
			return 1.75f;
		return 1;
	}
}
