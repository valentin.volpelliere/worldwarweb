package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;

@Entity
public class Plane extends UnitAir{
	public Plane()
	{
		super();
	}
	public Plane(Player owner,int x,int y) {
		super(owner, "plane", 72,x,y, 5, 1, 300, 110,350,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitAir)
			return 1.5f;
		return 0;
	}
}
