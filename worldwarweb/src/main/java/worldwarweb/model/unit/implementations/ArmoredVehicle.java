package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.Vehicle;

@Entity
public class ArmoredVehicle extends Vehicle{
	public ArmoredVehicle()
	{
		super();
	}
	public ArmoredVehicle(Player owner,int x,int y) {
		super(owner, "armoredVehicle", 66,x,y, 5, 0, 1000, 0,650,0,2);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
}
