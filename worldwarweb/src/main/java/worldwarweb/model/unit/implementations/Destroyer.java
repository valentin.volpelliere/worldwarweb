package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitGround;

@Entity
public class Destroyer extends UnitGround {
	public Destroyer() {
		super();
	}
	public Destroyer(Player owner,int x,int y) {
		super(owner, "destroyer", 65,x,y, 3, 1, 550, 230,1000,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof Archer || enemy instanceof Soldier || enemy instanceof Car)
			return 1.5f;
		return 1;
	}
}
