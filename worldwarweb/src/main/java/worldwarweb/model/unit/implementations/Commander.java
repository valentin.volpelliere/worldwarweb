package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.tile.implementations.Airport;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;

@Entity
public class Commander extends UnitGround{
	
	public Commander()
	{
		super();
	}
	public Commander(Player owner,int x,int y) {
		super(owner, "commander", 68,x,y, 5, 1, 800, 240,0,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(!(enemy instanceof UnitAir))
			return 1.25f;
		return 0;
	}
}
