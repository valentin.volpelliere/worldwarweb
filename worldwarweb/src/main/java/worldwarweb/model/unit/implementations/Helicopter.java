package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;

@Entity
public class Helicopter extends UnitAir{
	public Helicopter()
	{
		super();
	}
	public Helicopter(Player owner,int x,int y) {
		super(owner, "helicopter", 74,x,y, 4, 2, 320, 130,500,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof UnitGround)
			return 1.25f;
		return 1;
	}
}
