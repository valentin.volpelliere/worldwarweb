package worldwarweb.model.unit.implementations;

import javax.persistence.Entity;

import worldwarweb.model.Player;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.Vehicle;

@Entity
public class Archer extends UnitGround {
	public Archer()
	{
		super();
	}
	public Archer(Player owner,int x,int y) {
		super(owner, "bazooka", 60,x,y, 2, 3, 160, 95,220,1);
		if(!(getOwner() ==null) && !(getOwner().getColor()==null)) {
			switch(getOwner().getColor().toUpperCase()) {
			case "RED":
				setTextureid(getTextureid());
				break;
			case "BLUE":
				setTextureid(getTextureid()+26);
				break;
			}
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		return super.clone(); 
	} 
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		if(enemy instanceof Vehicle)
			return 1.5f;
		if(enemy instanceof UnitAir)
			return 1.25f;
		return 1;
	}
}
