package worldwarweb.model.unit;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import javassist.expr.Instanceof;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.implementations.Commander;

@Entity
public abstract class Vehicle extends Unit{
	@OneToMany(mappedBy="passenger",cascade=CascadeType.ALL,orphanRemoval = false)
	private List<Unit> units;
	
	private int nb_seat;
	
	public Vehicle(){
		super();
	}
	
	public Vehicle(Player owner, String name, int textureid, int x,int y, int range_mvmt, int range_atk, int hp,int atk,int cost, int max_attack_per_round,int nb_seat) {
		super(owner, name, textureid, x,y, range_mvmt, range_atk, hp, atk,cost, max_attack_per_round);
		this.units = new ArrayList<>();
		this.nb_seat = nb_seat;
	}
	
	public boolean load(Unit unit) {
		if(units.size() < nb_seat) {
			if(!(unit instanceof Vehicle) && (unit instanceof UnitGround)) {
				unit.setX(-1);
				unit.setY(-1);
				unit.setPassenger(this);
				this.units.add(unit);
				
				return true;
			}
		}
		return false;
	}
	
	public boolean canDropAt(Tile tile) {
		if(units.size() == 0)
			return false;
		return tile.getSpeedCoef(units.get(0)) > 0;
	}
	
	public void unload(Tile t) {
		if(units.size() > 0) {
			Unit unit = units.get(0);
			int x2 = this.getX(),
					x1 = t.getX(),
					y2 = this.getY(),
					y1 = t.getY();
			boolean north = x2 == x1 && y2 == y1-1 && t.getGame().getTileAt(x1, y1-1) != null,
					south = x2 == x1 && y2 == y1+1 && t.getGame().getTileAt(x1, y1+1) != null,
					east = x2 == x1+1 && y2 == y1  && t.getGame().getTileAt(x1+1, y1) != null,
					west = x2 == x1-1 && y2 == y1  && t.getGame().getTileAt(x1-1, y1) != null;
			if((north || south || east || west) && canDropAt(t)) {
				unit.setX(t.getX());
				unit.setY(t.getY());
				unit.setPassenger(null);
				this.units.remove(unit);
			}
		}
	}
	
	public void cEstBallotVousDecedez() {
		for(Unit unit : units) {
			unit.setPassenger(null);
			unit.getOwner().getUnits().remove(unit);
			unit.setOwner(null);
			this.units.remove(unit);
			if(unit instanceof Commander) {
				//LOL T'A PERDU
			}
		}
	}
	
	@Override
	public float getAttackEfficiency(Unit enemy) {
		return 0;
	}

	public List<Unit> getUnits() {
		return units;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

	public int getNb_seat() {
		return nb_seat;
	}

	public void setNb_seat(int nb_seat) {
		this.nb_seat = nb_seat;
	}
}
