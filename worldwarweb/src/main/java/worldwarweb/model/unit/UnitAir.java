package worldwarweb.model.unit;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import worldwarweb.model.Player;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class UnitAir extends Unit{
	public UnitAir()
	{
		super();
	}
	
	public UnitAir(Player owner, String name, int textureid, int x,int y, int range_mvmt, int range_atk, int hp,int atk,int cost, int max_attack_per_round) {
		super(owner, name, textureid, x,y, range_mvmt, range_atk, hp, atk,cost, max_attack_per_round);
	}

	@Override
	public float getAttackEfficiency(Unit enemy) {
		/*
		 * if(enemy instanceOf(UnitWater))
		 * 		return 1.75;
		 */
		return 1;
	}
}
