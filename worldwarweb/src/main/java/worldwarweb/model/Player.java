package worldwarweb.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import worldwarweb.model.tile.Building;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;

@Entity
@Table(name="player")
public class Player {

	@Id @Column
	@GeneratedValue(generator = "seqPlayer")
	@SequenceGenerator(name = "seqPlayer", sequenceName = "seq_player" ,allocationSize=1 )
	private Long id;
	
	@Column(nullable=false)
	private boolean isDead;
	
	@Column(nullable=false)
	private boolean isYourTurn;
	
	@Column(length=60,nullable=false)
	private String name;
	
	@Column(length=40,nullable=false)
	private int money;
	
	@Column(length=60,nullable=false)
	private String color;
	
	@OneToMany(mappedBy="owner",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Unit> units;
	
	@OneToMany(mappedBy="owner",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Building> building;
	
	@OneToOne(mappedBy="Winner",cascade=CascadeType.ALL,orphanRemoval = true)
	private Game game_actual;
	
	@Column(nullable=true)
	private Long selectionId;
	
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Game game;
	
	public Player(String name, List<Unit> units, List<Building> building, int money, String color, Game game, User user) {
		super();
		this.name = name;
		this.units = units;
		this.building=building;
		this.isYourTurn = false;
		this.money = money;
		this.color = color;
		this.game = game;
		this.user = user;
	}
	
	public Player() {
		super();
	}
	
	public boolean isYourTurn() {
		return isYourTurn;
	}

	public void setYourTurn(boolean isYourTurn) {
		this.isYourTurn = isYourTurn;
	}
	
	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void gainMoney() {
		setMoney(money + getMoneyPerTurn());
	}
	
	public int getMoneyPerTurn() {
		int sum = 0;
		for(Building b : building) {
			sum += b.generateMoney();
		}
		return sum;
	}

	
	@Override
	public String toString() {
		return "Player [name=" + name + ", units=" + units + ", money=" + money + ", color=" + color + "]";
	}

	public Long getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(Long selectionId) {
		this.selectionId = selectionId;
	}

	public Long getId() {
		return this.id;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Unit> getUnits() {
		return units;
	}
	public void setUnits(List<Unit> units) {
		this.units = units;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public List<Building> getBuilding() {
		return building;
	}

	public void setBuilding(List<Building> building) {
		this.building = building;
	}

	public Game getGame_actual() {
		return game_actual;
	}

	public void setGame_actual(Game game_actual) {
		this.game_actual = game_actual;
	}

	
	

	
	
	
	
}
