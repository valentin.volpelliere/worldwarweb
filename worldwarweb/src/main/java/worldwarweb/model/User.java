package worldwarweb.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import org.springframework.security.core.userdetails.UserDetails;

import worldwarweb.model.unit.Unit;

@Entity
@Table(name="\"user\"")
public class User implements UserDetails {
	

	private static final long serialVersionUID = -5405293332495309683L;

	@Id @Column
	@GeneratedValue(generator = "seqUser")
	@SequenceGenerator(name = "seqUser", sequenceName = "seq_user" ,allocationSize=1 )
	private Long id;
	
	@Column (length= 60, nullable=false)
	private String pseudo;
	@Column(length=60,nullable=false)
	private String username;
	
	@Column(length=60,nullable=false)
	private String password;
	
	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Authority> authorities;

	@ManyToMany
	private List<User> friends;
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<User>requestfriends_receive;
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Game>requestgames_receive;
	
	@OneToMany(mappedBy="user",cascade=CascadeType.ALL,orphanRemoval = true)
	private List<Player> players;


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}



	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Collection<Authority> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(Collection<Authority> authorities) {
		this.authorities = authorities;
	}

	public List<User> getFriends() {
		return friends;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}
	
	public List<User> getRequestfriends_receive() {
		return requestfriends_receive;
	}

	public void setRequestfriends_receive(List<User> requestfriends_receive) {
		this.requestfriends_receive = requestfriends_receive;
	}

	public List<Game> getRequestgames_receive() {
		return requestgames_receive;
	}

	public void setRequestgames_receive(List<Game> requestgames_receive) {
		this.requestgames_receive = requestgames_receive;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (pseudo == null) {
			if (other.pseudo != null)
				return false;
		} else if (!pseudo.equals(other.pseudo))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	
	
	
	
	
	
	
	
	
	
}
