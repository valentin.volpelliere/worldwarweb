package worldwarweb.model;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import worldwarweb.model.tile.Tile;
import worldwarweb.model.unit.Unit;

@Entity
@Table(name="game")
public class Game {
	@Id @Column
	@GeneratedValue(generator = "seqGame")
	@SequenceGenerator(name = "seqGame", sequenceName = "seq_game" ,allocationSize=1 )
	private Long id;
	
	
	@OneToMany(mappedBy="game",cascade=CascadeType.ALL,orphanRemoval = true)
	@OrderBy("y ASC, x ASC")
	private List<Tile> grid;
	
	@OneToMany(mappedBy="game",cascade=CascadeType.ALL,orphanRemoval = true)
	@OrderBy("id ASC")
	private List<Player> players =new ArrayList<Player>();
	@OneToOne
	private Player Winner;
	@Column(length = 40,nullable=false)
	private int nb_turns;
	@Column(length = 60,nullable=false)
	private LocalDateTime start_time;
	@Column(length = 60,nullable=true)
	private LocalDateTime end_time;
	@Column(length = 60,nullable=true)
	private LocalDateTime last_update;
	@Column(length = 40,nullable=false)
	private int grid_width;
	@Column(length = 40,nullable=false)
	private int grid_height;
	
	@Column(length = 40,nullable=false)
	private long last_player_id = 0;
	
	
	public Game(List<Tile> grid, List<Player> players,int grid_width,int grid_height) {
		super();
		this.grid = grid;
		this.players = players;
		this.grid_width = grid_width;
		this.grid_height = grid_height;
		Winner = null;
		this.nb_turns = 0;
		this.start_time = LocalDateTime.now();
		this.last_update = LocalDateTime.now();
	}
	
	public Game() {
		Winner = null;
		this.nb_turns = 0;
		this.start_time = LocalDateTime.now();
	}
	
	public LocalDateTime getEnd_time() {
		return end_time;
	}

	public void setEnd_time(LocalDateTime end_time) {
		this.end_time = end_time;
	}

	public String getDataHTML () {
		String ret = "";
		
		for(int i=0;i<grid.size();i++) {
			Tile stile = grid.get(i);
			Unit sunit = null;
			if(players != null)
			for(Player player : players) {
				for(Unit unit : player.getUnits()) {
					if(unit.getY()*grid_width+unit.getX() == i) {
						sunit = unit;
						break;
					}
				}
			}
			
			ret+= stile.toDataHTML();
			ret+= (sunit == null)? "" : ";" + sunit.toDataHTML();
			if((i+1)%grid_width==0) {
				ret+="\n";
			}
			else if(i != grid.size()-1) {
				ret+="|";
			}
		}
		
		return ret;
	}
	
	public Tile getTileAt (int x,int y) {
		if(y >= 0 && y < grid_height && x >= 0 && x < grid_width) {
			return grid.get(y*grid_width+x);
		}
		return null;
	}
	
	public void setTileAt (Tile tile) {
		if(tile.getY() >= 0 && tile.getY() < grid_height && tile.getX() >= 0 && tile.getX() < grid_width) {
			this.grid.set(tile.getY()*grid_width+tile.getX(), tile);
		}
	}
	
	@Override
	public String toString() {
		return "Game [grid=" + grid + ", players=" + players + ", Winner=" + Winner + ", nb_turns=" + nb_turns + ", start_time=" + start_time + "]";
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Tile> getGrid() {
		return grid;
	}


	public void setGrid(List<Tile> grid) {
		this.grid = grid;
	}


	public List<Player> getPlayers() {
		return players;
	}


	public void setPlayers(List<Player> players) {
		this.players = players;
	}


	public Player getWinner() {
		return Winner;
	}


	public void setWinner(Player winner) {
		Winner = winner;
	}


	public int getNb_turns() {
		return nb_turns;
	}


	public void setNb_turns(int nb_turns) {
		this.nb_turns = nb_turns;
	}


	public LocalDateTime getStart_time() {
		return start_time;
	}


	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}

	public int getGrid_width() {
		return grid_width;
	}

	public void setGrid_width(int grid_width) {
		this.grid_width = grid_width;
	}

	public int getGrid_height() {
		return grid_height;
	}

	public void setGrid_height(int grid_height) {
		this.grid_height = grid_height;
	}
	
	public LocalDateTime getLast_update() {
		return last_update;
	}
	
	public String getLastUpdateString() {
		return last_update.format(DateTimeFormatter.ISO_DATE_TIME);
	}

	public void setLast_update(LocalDateTime last_update) {
		this.last_update = last_update;
	}

	public void removeAllSelected() {
		for(Tile tile : grid) {
			tile.setSelect_tex_id(0);
		}
	}

	public long getLast_player_id() {
		return last_player_id;
	}

	public void setLast_player_id(long last_player_id) {
		this.last_player_id = last_player_id;
	}
	
	
	
}
