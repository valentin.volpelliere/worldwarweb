package worldwarweb.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import worldwarweb.dto.SessionDto;
import worldwarweb.form.UserForm;
import worldwarweb.model.User;
import worldwarweb.repository.AuthorityRepository;
import worldwarweb.repository.UserRepository;
import worldwarweb.service.GameService;
import worldwarweb.service.UserService;

@Controller
public class UserController {
		
	@Autowired
	private AuthorityRepository authorityrepository;
	
	@Autowired 
	private UserRepository userrepository;
	
	@Autowired
	private SessionDto session;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserService userservice;
	
	/**
	 * 
	 *SignIn of the website GET
	 */
	@GetMapping("/SignIn")
	public String signin(Model model) {
	UserForm form=new UserForm();
	model.addAttribute("user",form);
		return "sign_in";
	}
	
	/**
	 * 
	 *SignIn of the website POST
	 */
	@PostMapping("/SignIn")
	public String signinForm(@ModelAttribute("user") UserForm form,Model model) {
		int verif=userservice.PseudoAlReadyExist(form.getPseudo());
		if(verif==0) {
		User u=new User();
		u.setUsername(form.getUsername());
		u.setPassword(passwordEncoder.encode(form.getPassword()));
		u.setPseudo(form.getPseudo());
		u.setAuthorities(Arrays.asList(authorityrepository.findByAuthority("ROLE_USER")));
		u.setFriends(new ArrayList<User>());
		userrepository.save(u);
		return"redirect:/";
		}
		else {
			model.addAttribute("user",form);
			model.addAttribute("erreur","Le Pseudo est déja existant ");
			return "sign_in";
		}
	}
	
	
	@GetMapping("/addfriends")
	public String addfriends(Model model) {
		UserForm form=new UserForm();
		model.addAttribute("friend",form);
		
		
		return "add-friends";
	}
	
	@PostMapping("/addfriends")
	public String addfriendsForm(@ModelAttribute("friends") UserForm form,Model model){
		User friend=new User();
		friend=userrepository.findByPseudo(form.getPseudo());
		if(friend==null) {
			System.out.println("USER A AJOUTER NON TROUVE");
			model.addAttribute("friend",form);
			model.addAttribute("erreur","L'utilisateur recherché n'existe pas");
			
		}
		else {
			Optional<User> optionalUser =userrepository.findById(session.getUser().getUserId());
			if (!optionalUser.isPresent()) {
				throw new RuntimeException("User not found");
			}
			User user=optionalUser.get();
			if(!user.equals(friend) && userservice.AlreadyFriend(user.getFriends(), friend.getPseudo())==0) {
				friend.getRequestfriends_receive().add(user);
				userrepository.save(friend);
				model.addAttribute("friend",form);
				model.addAttribute("succes","La demande d'amis à bien été envoyé");
				
			}
			else {
				model.addAttribute("friend",form);
					if(userservice.AlreadyFriend(user.getFriends(), friend.getPseudo())==0) {
						model.addAttribute("erreur","L'utilisateur recherché ne peut pas être vous");
					}
					else {
						model.addAttribute("erreur","L'utilisateur "+ friend.getPseudo() + " est déja dans votre liste d'amis");
					}
				}
				
			}
		return "add-friends";
	}
	
	@GetMapping("/answerfriend/{id_friend}")
	public String answerRequestFriend(@PathVariable(required=true) Long id_friend, @RequestParam String action) {
		User friend=userrepository.findById(id_friend).get();
		User user=userrepository.findById(session.getUser().getUserId()).get();
		if(friend!=null && user!=null){
			//Si on accepte la demande d'amis on ajoute dans les 2 listes d'amis le nouvel ami
			if(action.equals("Y")) {
					user.getFriends().add(friend);
					friend.getFriends().add(user);
				}
			
			//On supprime la requete
			user.getRequestfriends_receive().remove(friend);
			userrepository.save(user);
		}
		
		return"redirect:/";
	}
	
	@GetMapping("/deletefriend/{id_friend}")
	public String deleteFriend(@PathVariable(required=true) Long id_friend) {
		User friend=userrepository.findById(id_friend).get();
		User user=userrepository.findById(session.getUser().getUserId()).get();
		if(friend!=null && user!=null){
			user.getFriends().remove(friend);
			friend.getFriends().remove(user);
			userrepository.save(user);
		}
		return"redirect:/";
	}
}
