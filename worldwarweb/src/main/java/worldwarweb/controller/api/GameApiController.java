package worldwarweb.controller.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import worldwarweb.dto.SessionDto;
import worldwarweb.model.Game;
import worldwarweb.repository.GameRepository;

@RestController
@RequestMapping("/api/game")
public class GameApiController {
	@Autowired
	private GameRepository gamesrepository;
	
	@Autowired
	private SessionDto session;
	
	private class SimpleGame{
		String date;
		long id;
		
		public SimpleGame(LocalDateTime date,long id) {
			this.date = date.format(DateTimeFormatter.ISO_DATE_TIME);
			this.id = id;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}
	}
	
	@GetMapping({"/"})
	public SimpleGame gameinfo() {
		Game game = gamesrepository.findById(session.getUser().getGameId()).get();
		SimpleGame s = new SimpleGame(game.getLast_update(), game.getId());
		return  s;
	}
}
