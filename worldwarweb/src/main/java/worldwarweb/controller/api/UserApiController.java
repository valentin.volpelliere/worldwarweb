package worldwarweb.controller.api;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import worldwarweb.dto.SessionDto;

import worldwarweb.model.User;
import worldwarweb.repository.UserRepository;


@RestController
@RequestMapping("/api/user")
public class UserApiController {
	
	@Autowired
	private UserRepository usersrepository;
	
	@Autowired 
	private SessionDto session;
	
	/*@GetMapping("/add")
	public UserDto addUser(@RequestParam Long UserId,@RequestParam String Pseudo) {
		UserDto user=new UserDto();
		user.setUserId(UserId);
		user.setPseudo(Pseudo);
		return session.getUser();
		
	}*/
	@GetMapping({"","/"})
	public Set<User> search(@RequestParam String q) {
		return usersrepository.findTop10ByPseudoLike("%" + q + "%",session.getUser().getPseudo());
	}
}
