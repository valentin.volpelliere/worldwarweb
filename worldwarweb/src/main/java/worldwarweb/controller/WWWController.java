package worldwarweb.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;



import worldwarweb.repository.GameRepository;
import worldwarweb.repository.PlayerRepository;
import worldwarweb.repository.UserRepository;
import worldwarweb.dto.SessionDto;
import worldwarweb.dto.UserDto;
import worldwarweb.form.GameruleForm;
import worldwarweb.form.PlayerForm;
import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.User;
import worldwarweb.model.tile.Building;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.*;
import worldwarweb.model.unit.*;
import worldwarweb.model.unit.implementations.*;
import worldwarweb.service.GameService;
import worldwarweb.service.UserService;

/**
 * controller for the website
 *
 */
@Controller
public class WWWController {
	

	@Autowired
	private GameRepository gamesrepository;
	
	@Autowired
	private PlayerRepository playerrepository;	
	
	@Autowired
	UserRepository userrepository;
	
	@Autowired
	private SessionDto session;
	
	/**
	 * 
	 *login of the website
	 */
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	
	/**
	 * index of the website
	 */
	@GetMapping("/")
	public String welcome(@AuthenticationPrincipal User user,Model model) {
		UserDto userdto=new UserDto();
		userdto.setUserDtoByUser(user);
		session.setUser(userdto);
		
		Long user_id = session.getUser().getUserId();
		model.addAttribute("current_user_id", user_id);
		model.addAttribute("games",gamesrepository.getFinishedGamesFromUser(user_id));
		model.addAttribute("friends",userrepository.findAllFriends(session.getUser().getUserId()));
		model.addAttribute("friends_requests",userrepository.findAllRequestFriends(session.getUser().getUserId()));
		model.addAttribute("pseudo", session.getUser().getPseudo());
		return "index";
	}
	

	@GetMapping("/list")
	public String list (Model model) {
		Long user_id = session.getUser().getUserId();

		if(user_id != null)
		{
			model.addAttribute("current_user_id", user_id);
			model.addAttribute("games",gamesrepository.getNotFinishedGamesFromUser(user_id));
			model.addAttribute("waiting_games",userrepository.findAllRequestGames(user_id));
			return "game-list";
		}
		return "redirect:/";
	} 
	
	@GetMapping({"/play","/join/{game_id}/{player_id}"})
	public String game_play(@PathVariable(required=false)Long game_id, @PathVariable(required=false)Long player_id, Model model) {
		
		//Retrieve game
		if(game_id != null && player_id != null)
		{
			session.getUser().setGameId(game_id);
			session.getUser().setPlayerId(player_id);
			return "redirect:/play";
		}
		if(session.getUser().getGameId() == null || session.getUser().getPlayerId() == null)
			return "redirect:/";
		Game game = gamesrepository.findById(session.getUser().getGameId()).get();
		Player player = playerrepository.findById(session.getUser().getPlayerId()).get();
		if(game == null || player == null || game.getWinner() != null)
			return "redirect:/";
		model.addAttribute("game", game);
		model.addAttribute("player", player);
		return "offcanvas";
	}

	@GetMapping({"/play/surrender"})
	public String game_surrender(Model model) {
		Game game = gamesrepository.getOne(session.getUser().getGameId());
		Player player = playerrepository.findById(session.getUser().getPlayerId()).get();
		User u=userrepository.findUserByRequestGame(session.getUser().getGameId());
		if(player != null && game != null && game.getWinner() == null)
		{
			player.setDead(true);
			player.getUnits().clear();
			for(Building currentBuilding : player.getBuilding())
			{
				currentBuilding.setOwner(null);
			}
			ArrayList<Player> players_alive = new ArrayList<Player>();
			for(Player currentPlayer : game.getPlayers())
			{	
				if(currentPlayer.getUser()==null) {
					currentPlayer.setDead(true);
					
				}
				if(currentPlayer.isDead() == false)
				{
					players_alive.add(currentPlayer);
				}
			}
			if(players_alive.size() <= 1)
			{
				if(players_alive.size() == 0)
				{
					u.getRequestgames_receive().remove(game);
					session.getUser().setGameId(null);
					session.getUser().setPlayerId(null);
					gamesrepository.delete(game);
					return "loose";
				}
				else
					game.setWinner(players_alive.get(0));
				game.setEnd_time(LocalDateTime.now());
			}
			
			gamesrepository.save(game);
			return "loose";
		}
		else
		{
			return "redirect:/";
		}
	}
	
	@GetMapping({"/play/pass"})
	public String game_pass_round(Model model) {
		Game game = gamesrepository.findById(session.getUser().getGameId()).get();
		Player player = playerrepository.findById(session.getUser().getPlayerId()).get();
		
		if(player != null && game != null && player.isDead() == false && game.getWinner() == null)
		{
			if(player.isYourTurn() == true)
			{	
				for(Tile tile : game.getGrid())
				{
					tile.setSelected(false);
					tile.setSelect_tex_id(0);
				}
				
				for(Building currentBuilding : player.getBuilding())
				{
					//heal 10 percent of all buildings
					currentBuilding.heal((int) (currentBuilding.getHpmax()*0.1));
				}
				
				for(Unit currentUnit : player.getUnits())
				{
					if(currentUnit instanceof Commander)
						currentUnit.heal((int)(currentUnit.getHpmax()*0.1));
					currentUnit.setRange_mvmt(currentUnit.getRange_mvmt_max());
					currentUnit.setNb_attack_round(0);
				}
				player.gainMoney();
				
				player.setYourTurn(false);
				
				boolean passToTheNextRound = false;
				
				if(game.getPlayers().lastIndexOf(player) == game.getPlayers().size()-1)
				{
					passToTheNextRound = true;
				}
				else
				{
					game.getPlayers().get(game.getPlayers().lastIndexOf(player)+1).setYourTurn(true);
					passToTheNextRound = false;
				}
				
				if(passToTheNextRound)
				{
					game.getPlayers().get(0).setYourTurn(true);
					game.setNb_turns(game.getNb_turns()+1);
				}
				game.setLast_update(LocalDateTime.now());
				game.setLast_player_id(1);
			}
			gamesrepository.save(game);
			return "redirect:/play";
		}
		else
		{
			return "redirect:/";
		}
	}
	
	@GetMapping({"/play/{selection_x}/{selection_y}","/play/{selection_x}/{selection_y}/{option_id}"})
	public String game_getSelectionAndProcess(@PathVariable(required=true)int selection_x,@PathVariable(required=true)int selection_y,@PathVariable(required=false)Long option_id, Model model) {
		
		//Retrieve game
		Game game = gamesrepository.findById(session.getUser().getGameId()).get();
		Player player = playerrepository.findById(session.getUser().getPlayerId()).get();
		if(game == null || player == null)
		{
			return "game-add";
		}
		
		if(selection_x == -1 && selection_y == -1)
		{
			for(Tile tile : game.getGrid())
			{
				tile.setSelected(false);
				tile.setSelect_tex_id(0);
			}
			
			player.setSelectionId(null);
			game.setLast_update(LocalDateTime.now());
			gamesrepository.save(game);
			return "redirect:/play";
		}
		
		if(player.isYourTurn())
		{
			for(Tile tile : game.getGrid())
			{
				tile.setSelected(false);
				tile.setSelect_tex_id(0);
			}
		}
		
		if(game.getWinner() == null)
		{
			//Player found
			if(player.isDead() == false)
			{
				
				//Get selected object & previously selected object
				Object targetSelection = null;
				Object previousSelection = null;
				
				//GET THE SELECTION & PREVIOUS SELECTION
				for(Player currentPlayer : game.getPlayers())
				{
					for(Unit currentUnit : currentPlayer.getUnits())
					{
						//If selection is a unit
						if(currentUnit.getX() == selection_x && currentUnit.getY() == selection_y)
						{
							targetSelection = currentUnit;
						}
						
						//If previous selection is a unit
						if(currentUnit.getId().equals(player.getSelectionId()))
						{
							previousSelection = currentUnit;
						}
					}
					
					for(Building currentBuilding : currentPlayer.getBuilding())
					{
						
						//If previous selection is a building
						if(currentBuilding.getId().equals(player.getSelectionId()))
						{
							previousSelection = currentBuilding;
						}
					}
				}
				
				for(Tile tile : game.getGrid())
				{
					if(tile instanceof Building)
					{
						if(tile.getX() == selection_x && tile.getY()==selection_y)
						{
							targetSelection = tile;
						}
					}
				}
				
				//If something was found
				if(previousSelection != null || targetSelection != null)
				{
					//Nothing found previously but target found
					if(previousSelection == null)
					{
						//If target is a unit
						if(targetSelection instanceof Unit)
						{
							Unit targetUnit = (Unit) targetSelection;
							//If targetSelection is owned by player set selection
							
							if(targetUnit.getOwner().getId().equals(player.getId()))
							{
								
								if(targetUnit.getNb_attack_round() < targetUnit.getMax_attack_per_round())
								{
									targetUnit.setTileSelectionBox();
									model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
									model.addAttribute("unitselected",targetSelection);
									player.setSelectionId(targetUnit.getId());
								}
								else
								{
									if(targetUnit instanceof Vehicle )
									{
										if(targetUnit.getRange_mvmt() == 0)
										{
											Tile north = game.getTileAt(targetUnit.getX(),targetUnit.getY()-1),
												south  = game.getTileAt(targetUnit.getX(),targetUnit.getY()+1),
												east   = game.getTileAt(targetUnit.getX()-1,targetUnit.getY()),
												west   = game.getTileAt(targetUnit.getX()+1,targetUnit.getY());
											boolean isNorthFree = true;
											boolean isSouthFree = true;
											boolean isEastFree = true;
											boolean isWestFree = true;
											for(Player currentPlayer : game.getPlayers())
											{
												for(Unit currentUnit : currentPlayer.getUnits())
												{
													if(north != null && currentUnit.getX() == north.getX() && currentUnit.getY() == north.getY())
														isNorthFree = false;
													if(south != null && currentUnit.getX() == south.getX() && currentUnit.getY() == south.getY())
														isSouthFree = false;
													if(east != null && currentUnit.getX() == east.getX() && currentUnit.getY() == east.getY())
														isEastFree = false;
													if(west != null && currentUnit.getX() == west.getX() && currentUnit.getY() == west.getY())
														isWestFree = false;
												}
											}
											if(north != null && isNorthFree && north.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(north))
												north.setSelect_tex_id(55);
											else if(north != null)
												north.setSelect_tex_id(0);
											if(south != null && isSouthFree && south.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(south)) 
												south.setSelect_tex_id(55);
											else if(south != null)
												south.setSelect_tex_id(0);
											if(east != null && isEastFree && east.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(east)) 
												east.setSelect_tex_id(55);
											else if(east != null)
												east.setSelect_tex_id(0);
											if(west != null&& isWestFree && west.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(west)) 
												west.setSelect_tex_id(55);
											else if(west != null)
												west.setSelect_tex_id(0);
											player.setSelectionId(targetUnit.getId());
										}
										else
										{
											targetUnit.setTileSelectionBox();
											player.setSelectionId(targetUnit.getId());
											model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
											model.addAttribute("unitselected",targetSelection);
										}
										
									}
								}
								model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
								model.addAttribute("unitselected",targetSelection);
								targetUnit.setTileSelectionBox();
								player.setSelectionId(targetUnit.getId());
							}
							else
							{
								model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
								model.addAttribute("unitselected",targetSelection);
								targetUnit.setTileSelectionBox();
								player.setSelectionId(targetUnit.getId());
							}
						}
						//If target is a building
						else if(targetSelection instanceof Building)
						{
							
							Building targetBuilding = (Building) targetSelection;
							//If targetSelection is owned by player set selection
							if(targetBuilding.getOwner() != null && targetBuilding.getOwner().getId().equals(player.getId()) && player.isYourTurn() == true)
							{
								if(targetBuilding.getProductible() != null && targetBuilding.getProductible().size() > 0)
								{
									List<Unit> buymenuitems = targetBuilding.getProductible();
									Tile north = game.getTileAt(targetBuilding.getX(),targetBuilding.getY()-1),
										south  = game.getTileAt(targetBuilding.getX(),targetBuilding.getY()+1),
										east   = game.getTileAt(targetBuilding.getX()-1,targetBuilding.getY()),
										west   = game.getTileAt(targetBuilding.getX()+1,targetBuilding.getY());
									boolean isNorthFree = true;
									boolean isSouthFree = true;
									boolean isEastFree = true;
									boolean isWestFree = true;
									for(Player currentPlayer : game.getPlayers())
									{
										for(Unit currentUnit : currentPlayer.getUnits())
										{
											if(north != null && currentUnit.getX() == north.getX() && currentUnit.getY() == north.getY())
												isNorthFree = false;
											if(south != null && currentUnit.getX() == south.getX() && currentUnit.getY() == south.getY())
												isSouthFree = false;
											if(east != null && currentUnit.getX() == east.getX() && currentUnit.getY() == east.getY())
												isEastFree = false;
											if(west != null && currentUnit.getX() == west.getX() && currentUnit.getY() == west.getY())
												isWestFree = false;
										}
									}
									for(Unit currentUnit : buymenuitems)
									{
										if(north == null || north.getSpeedCoef(currentUnit) <= 0.1)
											isNorthFree = false;
										if(south == null || south.getSpeedCoef(currentUnit) <= 0.1)
											isSouthFree = false;
										if(east == null || east.getSpeedCoef(currentUnit) <= 0.1)
											isEastFree = false;
										if(west == null || west.getSpeedCoef(currentUnit) <= 0.1)
											isWestFree = false;
									}
									if(north != null && isNorthFree == true)
										north.setSelect_tex_id(55);
									else if(north != null)
										north.setSelect_tex_id(0);
									if(south != null && isSouthFree== true) 
										south.setSelect_tex_id(55);
									else if(south != null)
										south.setSelect_tex_id(0);
									if(east != null && isEastFree == true) 
										east.setSelect_tex_id(55);
									else if(east != null)
										east.setSelect_tex_id(0);
									if(west != null&& isWestFree == true) 
										west.setSelect_tex_id(55);
									else if(west != null)
										west.setSelect_tex_id(0);
									model.addAttribute("buymenuitems",buymenuitems);
									player.setSelectionId(targetBuilding.getId());
								}
								else
								{
									player.setSelectionId(null);
								}
							}
							else
							{
								player.setSelectionId(null);
							}
						}
					}
					
					//Nothing found on target but previously found
					else if(targetSelection == null)
					{
						//If previous selection is a building, maybe a buy
						if(previousSelection instanceof Building)
						{
							Building previousBuilding = (Building) previousSelection;
							boolean isTargetFree = true;
							for(Player currentPlayer : game.getPlayers())
							{
								for(Unit currentUnit : currentPlayer.getUnits())
								{
									if(currentUnit.getX() == selection_x && currentUnit.getY() == selection_y)
										isTargetFree = false;
								}
							}
							
							if(option_id != null && isTargetFree && game.getTileAt(selection_x, selection_y) != null && previousBuilding.isOwner(player) && player.isYourTurn() == true)
							{
								Tile t = game.getTileAt(selection_x, selection_y);
								Unit newUnit = previousBuilding.getProductible().get(option_id.intValue());
								if(t.getSpeedCoef(newUnit) > 0.1)
									previousBuilding.buyUnit(option_id.intValue(), game.getTileAt(selection_x, selection_y), gamesrepository.getNextValTileAndUnit());
							}
							player.setSelectionId(null);
						}
						//If previous selection is a unit maybe a movement
						else if(previousSelection instanceof Unit)
						{
							Unit selectedUnit = (Unit) previousSelection;
							//Enable movement if player is owner
							if(selectedUnit.getOwner().getId().equals(player.getId()) && player.isYourTurn() == true)
							{
								if(selectedUnit.getRange_mvmt() == 0 && selectedUnit instanceof Vehicle)
								{
									if(	selection_x == selectedUnit.getX()-1 && selection_y == selectedUnit.getY() ||
										selection_x == selectedUnit.getX() && selection_y == selectedUnit.getY()+1 ||
										selection_x == selectedUnit.getX()+1 && selection_y == selectedUnit.getY() ||
										selection_x == selectedUnit.getX() && selection_y == selectedUnit.getY()-1)
									{
										Vehicle vehicle = (Vehicle) selectedUnit;
										vehicle.unload(game.getTileAt(selection_x, selection_y));
										player.setSelectionId(null);
									}
								}
								else
								{
									if(selectedUnit.getAccessibleTiles().contains(game.getTileAt(selection_x, selection_y)))
									{
										selectedUnit.setX(selection_x);
										selectedUnit.setY(selection_y);
										
										//Tu vas faire quoi maintenant ? Te déplacer en fauteuille roullant ?
										selectedUnit.pafFauteuilleRoulant();
									}
									else
									{
										model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
									}
								}
							}
							else
							{
								model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
							}
							player.setSelectionId(null);
						}
					}
					//Found previous selection & target
					else
					{
						
						//If previous selection is a building replace the selection to target
						if(previousSelection instanceof Building)
						{
							
							//If target is a unit
							if(targetSelection instanceof Unit)
							{
								Unit targetUnit = (Unit) targetSelection;
								player.setSelectionId(targetUnit.getId());
								model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
								model.addAttribute("unitselected",targetSelection);
								targetUnit.setTileSelectionBox();
							}
							//If target is a building
							else if(targetSelection instanceof Building)
							{
								Building previousBuilding = (Building) previousSelection;
								Building targetBuilding = (Building) targetSelection;
								//If targetSelection is owned by player, set selection
								if(targetBuilding.getOwner() != null && targetBuilding.getOwner().getId().equals(player.getId()))
								{
									if(previousBuilding.getId().equals(targetBuilding.getId()))
									{
										player.setSelectionId(null);
									}
									else
									{
										if(targetBuilding.getProductible() != null && targetBuilding.getProductible().size() > 0 && targetBuilding.isOwner(player) && player.isYourTurn() == true)
										{
											List<Unit> buymenuitems = targetBuilding.getProductible();
											model.addAttribute("buymenuitems",buymenuitems);
											player.setSelectionId(targetBuilding.getId());
										}
										else
										{
											player.setSelectionId(null);
										}
									}
								}
							}
						}
						//If previous selection is a unit maybe an attack or a selection replacement
						else if(previousSelection instanceof Unit)
						{
							
							//If target is a unit
							if(targetSelection instanceof Unit)
							{
								Unit selectedUnit = (Unit) previousSelection;
								Unit targetUnit = (Unit) targetSelection;
								//If targetSelection is owned by player set selection
								if(targetUnit.getOwner().getId().equals(player.getId()))
								{
									
									if(selectedUnit.getId().equals(targetUnit.getId()))
									{
										player.setSelectionId(null);
									}
									else
									{
										
										if(targetUnit instanceof Vehicle)
										{
											Vehicle vehicle = (Vehicle) targetUnit;
											if(selectedUnit.getAccessibleTiles().contains(game.getTileAt(vehicle.getX(), vehicle.getY())) && player.isYourTurn() == true)
											{
												if(vehicle.load(selectedUnit))
												{
													selectedUnit.pafFauteuilleRoulant();
													player.setSelectionId(null);
												}
												else
												{
													targetUnit.setTileSelectionBox();
													model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
													model.addAttribute("unitselected",targetSelection);
													player.setSelectionId(targetUnit.getId());
												}
											}
											else
											{
												if(targetUnit.getRange_mvmt() == 0)
												{
													Tile north = game.getTileAt(targetUnit.getX(),targetUnit.getY()-1),
														south  = game.getTileAt(targetUnit.getX(),targetUnit.getY()+1),
														east   = game.getTileAt(targetUnit.getX()-1,targetUnit.getY()),
														west   = game.getTileAt(targetUnit.getX()+1,targetUnit.getY());
													boolean isNorthFree = true;
													boolean isSouthFree = true;
													boolean isEastFree = true;
													boolean isWestFree = true;
													for(Player currentPlayer : game.getPlayers())
													{
														for(Unit currentUnit : currentPlayer.getUnits())
														{
															if(north != null && currentUnit.getX() == north.getX() && currentUnit.getY() == north.getY())
																isNorthFree = false;
															if(south != null && currentUnit.getX() == south.getX() && currentUnit.getY() == south.getY())
																isSouthFree = false;
															if(east != null && currentUnit.getX() == east.getX() && currentUnit.getY() == east.getY())
																isEastFree = false;
															if(west != null && currentUnit.getX() == west.getX() && currentUnit.getY() == west.getY())
																isWestFree = false;
														}
													}
													if(north != null && isNorthFree && north.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(north))
														north.setSelect_tex_id(55);
													else if(north!=null)
														north.setSelect_tex_id(0);
													if(south != null && isSouthFree && south.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(south)) 
														south.setSelect_tex_id(55);
													else if(south!=null)
														south.setSelect_tex_id(0);
													if(east != null && isEastFree && east.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(east)) 
														east.setSelect_tex_id(55);
													else if(east!=null)
														east.setSelect_tex_id(0);
													if(west != null&& isWestFree && west.getSpeedCoef(targetUnit) > 0.1 && ((Vehicle)targetUnit).canDropAt(west)) 
														west.setSelect_tex_id(55);
													else if(west!=null)
														west.setSelect_tex_id(0);
													player.setSelectionId(targetUnit.getId());
												}
												else
												{
													targetUnit.setTileSelectionBox();
													
													model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
													model.addAttribute("unitselected",targetSelection);
													player.setSelectionId(targetUnit.getId());
												}
											}
										}
										else
										{
											model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
											model.addAttribute("unitselected",targetSelection);
											targetUnit.setTileSelectionBox();
											player.setSelectionId(targetUnit.getId());
										}
										model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
										model.addAttribute("unitselected",targetSelection);
										targetUnit.setTileSelectionBox();
										player.setSelectionId(targetUnit.getId());
									}
								}
								else
								{
									if(selectedUnit.getOwner().getId().equals(player.getId()) == false)
									{
										model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
										model.addAttribute("unitselected",targetSelection);
										targetUnit.setTileSelectionBox();
										player.setSelectionId(targetUnit.getId());
									}
									else
									{
										//If it is a legit attack against a unit
										ArrayList<Tile> tmp = new ArrayList<Tile>();
										selectedUnit.getAttackableTiles(tmp, selectedUnit.getX(), selectedUnit.getY());
										boolean canAttackWithoutMoving = tmp.contains(game.getTileAt(targetUnit.getX(), targetUnit.getY()));
										if(selectedUnit.getAllAttackableTiles(selectedUnit.getAccessibleTiles()).contains(game.getTileAt(targetUnit.getX(), targetUnit.getY())) && (selectedUnit.getRange_mvmt() == 0 || canAttackWithoutMoving) && selectedUnit.getNb_attack_round() < selectedUnit.getMax_attack_per_round() && selectedUnit.getAttackEfficiency(targetUnit) > 1 && player.isYourTurn() == true)
										{
											selectedUnit.attack(targetUnit);
											Player winner = game.getWinner();
											if(winner != null)
											{
												if(player == winner)
													return "win";
												else
													return "loose";
											}
											
											selectedUnit.pafFauteuilleRoulant();
											selectedUnit.setNb_attack_round(selectedUnit.getNb_attack_round()+1);
											player.setSelectionId(null);
										}
										else
										{
											player.setSelectionId(targetUnit.getId());
											model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
											model.addAttribute("unitselected",targetSelection);
											targetUnit.setTileSelectionBox();
										}
									}
								}
							}
							//If target is a building
							else if(targetSelection instanceof Building)
							{
								Unit selectedUnit = (Unit) previousSelection;
								Building targetBuilding = (Building) targetSelection;
								//If targetSelection is owned by player set selection
								if(targetBuilding.getOwner() != null && targetBuilding.getOwner().getId() == player.getId())
								{
									if(targetBuilding.getProductible() != null && targetBuilding.getProductible().size() > 0 && targetBuilding.isOwner(player) && player.isYourTurn() == true)
									{
										List<Unit> buymenuitems = targetBuilding.getProductible();
										Tile north = game.getTileAt(targetBuilding.getX(),targetBuilding.getY()-1),
											south  = game.getTileAt(targetBuilding.getX(),targetBuilding.getY()+1),
											east   = game.getTileAt(targetBuilding.getX()-1,targetBuilding.getY()),
											west   = game.getTileAt(targetBuilding.getX()+1,targetBuilding.getY());
										boolean isNorthFree = true;
										boolean isSouthFree = true;
										boolean isEastFree = true;
										boolean isWestFree = true;
										for(Player currentPlayer : game.getPlayers())
										{
											for(Unit currentUnit : currentPlayer.getUnits())
											{
												if(north != null && currentUnit.getX() == north.getX() && currentUnit.getY() == north.getY())
													isNorthFree = false;
												if(south != null && currentUnit.getX() == south.getX() && currentUnit.getY() == south.getY())
													isSouthFree = false;
												if(east != null && currentUnit.getX() == east.getX() && currentUnit.getY() == east.getY())
													isEastFree = false;
												if(west != null && currentUnit.getX() == west.getX() && currentUnit.getY() == west.getY())
													isWestFree = false;
											}
										}

										for(Unit currentUnit : buymenuitems)
										{
											if(north == null || north.getSpeedCoef(currentUnit) <= 0.1)
												isNorthFree = false;
											if(south == null || south.getSpeedCoef(currentUnit) <= 0.1)
												isSouthFree = false;
											if(east == null || east.getSpeedCoef(currentUnit) <= 0.1)
												isEastFree = false;
											if(west == null || west.getSpeedCoef(currentUnit) <= 0.1)
												isWestFree = false;
										}
										if(north != null && isNorthFree == true)
											north.setSelect_tex_id(55);
										else if(north!=null)
											north.setSelect_tex_id(0);
										if(south != null && isSouthFree== true) 
											south.setSelect_tex_id(55);
										else if(south!=null)
											south.setSelect_tex_id(0);
										if(east != null && isEastFree == true) 
											east.setSelect_tex_id(55);
										else if(east!=null)
											east.setSelect_tex_id(0);
										if(west != null && isWestFree == true) 
											west.setSelect_tex_id(55);
										else if(west!=null)
											west.setSelect_tex_id(0);
										model.addAttribute("buymenuitems",buymenuitems);
										player.setSelectionId(targetBuilding.getId());
									}
									else
									{
										player.setSelectionId(null);
									}
									
								}
								else
								{
									if(selectedUnit.getOwner().getId().equals(player.getId()) == true)
									{
										//If it is a legit attack against a building
										ArrayList<Tile> tmp = new ArrayList<Tile>();
										selectedUnit.getAttackableTiles(tmp, selectedUnit.getX(), selectedUnit.getY());
										boolean canAttackWithoutMoving = tmp.contains(game.getTileAt(targetBuilding.getX(), targetBuilding.getY()));
										if(selectedUnit.getAllAttackableTiles(selectedUnit.getAccessibleTiles()).contains(game.getTileAt(targetBuilding.getX(), targetBuilding.getY()))  && (selectedUnit.getRange_mvmt() == 0 || canAttackWithoutMoving) && selectedUnit.getNb_attack_round() < selectedUnit.getMax_attack_per_round() && player.isYourTurn() == true)
										{
											if(selectedUnit.attack(targetBuilding))
											{
												Player winner = game.getWinner();
												if(winner != null)
												{
													if(player == winner)
														return "win";
													else
														return "loose";
												}
												selectedUnit.pafFauteuilleRoulant();
												selectedUnit.setNb_attack_round(selectedUnit.getNb_attack_round()+1);
											}
											
										}
										else
										{
											player.setSelectionId(null);
										}
									}
								}
							}
							
						}
					}
					
				}
				else
				{
					if(game.getTileAt(selection_x, selection_y) != null)
						model.addAttribute("tileselected",game.getTileAt(selection_x, selection_y));
				}
			}
			
			//savoir si il faut notifier le joueur que c'est son tour :
			boolean notify_client = game.getLast_player_id() == 1 && player.isYourTurn();
			game.setLast_player_id(0);
			if(player.isYourTurn() == true  && selection_x != -1) {
				game.setLast_update(LocalDateTime.now());
			}
			
			
			ArrayList<Unit> units = new ArrayList<>();
			units.add(new Archer(null,-1,-1));
			units.add(new ArmoredVehicle(null,-1,-1));
			units.add(new Barge(null,-1,-1));
			units.add(new BattleShip(null,-1,-1));
			units.add(new Boat(null,-1,-1));
			units.add(new Car(null,-1,-1));
			units.add(new Commander(null,-1,-1));
			units.add(new Destroyer(null,-1,-1));
			units.add(new Helicopter(null,-1,-1));
			units.add(new Plane(null,-1,-1));
			units.add(new Soldier(null,-1,-1));
			units.add(new StealthPlane(null,-1,-1));
			units.add(new Submarine(null,-1,-1));
			units.add(new Tank(null,-1,-1));
			model.addAttribute("unitlist",units);
			playerrepository.save(player);
			gamesrepository.save(game);
			model.addAttribute("notifyTurn",notify_client);
			model.addAttribute("game", game);
			model.addAttribute("player",player);
			return "offcanvas";
		}
		else
		{
			return "redirect:/";
		}
		
	}
	
	@GetMapping("/create")
	public String create(Model model) {
		GameruleForm form=new GameruleForm();
		form.setListmaps(GameService.getListFile("./src/main/resources/static/csv","csv"));
		form.setUser(session.getUser().getUserId());
		model.addAttribute("games",form);
		model.addAttribute("friends",userrepository.findAllFriends(session.getUser().getUserId()));
		
		return "game-add";
	}
	
	
	@PostMapping("/create")
	public String createForm(@ModelAttribute("games") GameruleForm form,Model model) {
		
		//On creer la game en fonction du form
			Game g=new Game();
			int coordonneeXBase[]=new int[4];
			int coordonneeYBase[]=new int[4];
			int cpt=0;
			int nbBase=GameService.GetNbBaseFromMap(form.getChoixmaps());
			int nbjoueur=nbBase-1;
			int locationCom[]=  new int[2];
			
			//SET des Players
			g.getPlayers().add(form.getPlayers());
			g.getPlayers().get(0).setUser(userrepository.findById(form.getUser()).get());
			g.getPlayers().get(0).setGame(g);
			
			g.getPlayers().get(0).setYourTurn(true);
			User users=new User();
			users.setFriends(form.getFriends().stream().map(e -> userrepository.findById(e).get()).collect(Collectors.toList()));
			for(User u: users.getFriends() )
			{
				u.getRequestgames_receive().add(g);
				g.getPlayers().add(new Player(u.getPseudo()+" - ⏱",new ArrayList<Unit>(),new ArrayList<Building>(),g.getPlayers().get(0).getMoney(),"blue",g,null));
			}
			
			//On verifie si le nombre de joueur n'est pas supérieur au nombre de base
			if(g.getPlayers().size()>nbBase) {
				model.addAttribute("games",form);
				model.addAttribute("friends",userrepository.findAllFriends(session.getUser().getUserId()));
				model.addAttribute("erreur","La map sélectionnée ne peut contenir que " + nbBase + " joueurs maximum");
				return "game-add";
				
			}
			//SET de la grille 
			g.setGrid(GameService.getTileListFromCSV("./src/main/resources/static/csv/" + form.getChoixmaps(),g.getPlayers()));
			g.setGrid_height(GameService.getHeightCSV("./src/main/resources/static/csv/"+ form.getChoixmaps()));
			g.setGrid_width(GameService.getWidthCSV("./src/main/resources/static/csv/"  + form.getChoixmaps()));
			for(int i=0;i<g.getGrid().size();i++) {
				g.getGrid().get(i).setGame(g);
				//Si la Tile est une Base on récupère les coordonnées de la Base pour définir la pos du commander
				if(g.getGrid().get(i) instanceof Base) {
						coordonneeXBase[cpt]=g.getGrid().get(i).getX();
						coordonneeYBase[cpt]=g.getGrid().get(i).getY();
					cpt++;
				}
			}
			
			//On verifie si le nombre de joueur n'est pas égale à 1
			if(g.getPlayers().size()==1) {
				
				model.addAttribute("games",form);
				model.addAttribute("friends",userrepository.findAllFriends(session.getUser().getUserId()));
				model.addAttribute("erreur","Selectionnez " + nbjoueur + " amis pour lancer la game");
				return "game-add";
			}
			
			
			//SET du Commander
				for(int i=0;i<g.getPlayers().size();i++) {
					g.getPlayers().get(i).setUnits(new ArrayList<Unit>());
					locationCom=GameService.getStartLocationCommander(coordonneeXBase[i], coordonneeYBase[i], g.getGrid_width()/2, g.getGrid_height()/2);
					g.getPlayers().get(i).getUnits().add(new Commander(g.getPlayers().get(i),locationCom[0],locationCom[1]));
				}
			g.setLast_update(LocalDateTime.now());
			g.setLast_player_id(0);
			gamesrepository.save(g);
			session.getUser().setGameId(g.getId());
			session.getUser().setPlayerId(g.getPlayers().get(0).getId());
			
			return "redirect:/list";
				
		}
	
	@GetMapping("/deletegamerequest/{id_game}")
	public String deleteGame_Request(@PathVariable(required = true)Long id_game) {
		Game g=gamesrepository.findById(id_game).get();
		User u=userrepository.findById(session.getUser().getUserId()).get();
		if(g!=null && u!=null) {
			u.getRequestgames_receive().remove(g);
			gamesrepository.delete(g);
		}
		
		
		
		
		return "redirect:/list";
	}
	
	@GetMapping("/join/{id_game}")
	public String JoinGame(@PathVariable(required=true)Long id_game,Model model) {
		ArrayList<String> listcouleurs=new ArrayList<String>();
		listcouleurs.add("Red");
		listcouleurs.add("Blue");
		//listcouleurs.add("Green");
		//listcouleurs.add("Yellow");
		Game g=gamesrepository.getOne(id_game);
		for(Player p: g.getPlayers()) {
			if(p.getUser()!=null) {
				listcouleurs.remove(p.getColor());
			}
		}
		PlayerForm form=new PlayerForm();
		model.addAttribute("player", form);
		model.addAttribute("couleurdispo", listcouleurs);
		session.getUser().setGameId(id_game);
		return "game-join";
	}
	
	@PostMapping("/join")
	public String JoinGameForm(@ModelAttribute("player") PlayerForm form) {
		Game g=gamesrepository.getOne(session.getUser().getGameId());
		User u=userrepository.findById(session.getUser().getUserId()).get();
		int nbplayer=0;
		if(g!=null && u!=null) {
			for(Player p: g.getPlayers()) {
				if(p.getUser()==null) {
					p.setName(form.getName());
					p.setColor(form.getColor());
					p.setUser(u);
					break;
				}
				nbplayer++;
			}
		}
		u.getRequestgames_receive().remove(g);
		gamesrepository.save(g);
		session.getUser().setPlayerId(g.getPlayers().get(nbplayer).getId());		
		return "redirect:/join/"+session.getUser().getGameId()+"/"+session.getUser().getPlayerId();
	}
	
	
	@GetMapping("/highscores")
	public String highScore(Model model) {
		List<User> users=userrepository.findAll();
		List<User>classementList=new ArrayList<User>();
		Long nbvictoire[]=new Long[users.size()];
		Long temp;
		int index=0;
		for(User u : users) {
			nbvictoire[index]=gamesrepository.countNbWinByUser(u.getId());
			if(index==0) {
				classementList.add(u);
			}
			else {
				if(nbvictoire[index]>nbvictoire[index-1]) {
					classementList.add(index-1,u);
					temp=nbvictoire[index-1];
					nbvictoire[index-1]=nbvictoire[index];
					nbvictoire[index]=temp;
					
				}
				else {
					classementList.add(u);
				}
			}
			index++;
			
		}
		model.addAttribute("classement", classementList);
		model.addAttribute("nbVictoire",nbvictoire);
		model.addAttribute("monclassement",classementList.indexOf(userrepository.findById(session.getUser().getUserId()).get())+1);
		
		return "highscores";
	}
	
	
	
}
