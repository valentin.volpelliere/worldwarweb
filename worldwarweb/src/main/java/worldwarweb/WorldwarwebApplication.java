package worldwarweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorldwarwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorldwarwebApplication.class, args);
	}

}
