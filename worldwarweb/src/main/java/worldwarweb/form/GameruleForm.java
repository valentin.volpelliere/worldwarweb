package worldwarweb.form;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import worldwarweb.model.Player;
import worldwarweb.model.User;

/**
 * class used to make a form for creating gamerules
 */
public class GameruleForm {
	private Long id;
	
	/**
	 * width of the grid
	 */
	@Min(1)	@Max(500)
	private int width;
	/**
	 * height of the grid
	 */
	@Min(1)	@Max(500)
	private int height;
	private int nb_turns;
	private LocalDateTime start_time;
	private int grid_width;
	private int grid_height;
	private Player players;
	private Set<Long>friends;
	private List<String> listmaps= new ArrayList<String>();
	private String choixmaps;
	private Long user;
	
	/*public void addPlayer() {
		this.players.add(new Player());
	}
	
	public void removePlayer() {
		this.players.remove(this.players.size()-1);
	}*/
	
	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}

	public int getNb_turns() {
		return nb_turns;
	}
	public void setNb_turns(int nb_turns) {
		this.nb_turns = nb_turns;
	}
	public LocalDateTime getStart_time() {
		return start_time;
	}
	public void setStart_time(LocalDateTime start_time) {
		this.start_time = start_time;
	}
	public int getGrid_width() {
		return grid_width;
	}
	public void setGrid_width(int grid_width) {
		this.grid_width = grid_width;
	}
	public int getGrid_height() {
		return grid_height;
	}
	public void setGrid_height(int grid_height) {
		this.grid_height = grid_height;
	}
	public Player getPlayers() {
		return players;
	}
	public void setPlayers(Player players) {
		this.players = players;
	}

	public List<String> getListmaps() {
		return listmaps;
	}

	public void setListmaps(List<String> listmaps) {
		this.listmaps = listmaps;
	}

	public String getChoixmaps() {
		return choixmaps;
	}

	public void setChoixmaps(String choixmaps) {
		this.choixmaps = choixmaps;
	}

	public Set<Long> getFriends() {
		return friends;
	}

	public void setFriends(Set<Long> friends) {
		this.friends = friends;
	}
	
	
	
	
	
	
	
	
	
}
