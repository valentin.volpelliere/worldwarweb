package worldwarweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import worldwarweb.model.Authority;


public interface AuthorityRepository extends JpaRepository<Authority, Long> {
	public Authority findByAuthority(String name_authority);
}
