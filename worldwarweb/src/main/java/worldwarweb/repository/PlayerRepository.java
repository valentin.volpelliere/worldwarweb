package worldwarweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import worldwarweb.model.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {
	
}
