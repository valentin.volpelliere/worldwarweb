package worldwarweb.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import worldwarweb.model.Game;

public interface GameRepository extends JpaRepository<Game, Long>{
	@Query(value = "SELECT nextval('seq_tile_and_unit');", nativeQuery = true)
    public Long getNextValTileAndUnit();
	
	@Query(value= "select * from game where winner_id is NULL and id in (select game_id from player where user_id=:user_id);", nativeQuery = true)
	public ArrayList<Game> getNotFinishedGamesFromUser(@Param("user_id") long user_id);
	
	@Query(value= "select * from game where winner_id is not NULL and id in (select game_id from player where user_id=:user_id);", nativeQuery = true)
	public ArrayList<Game> getFinishedGamesFromUser(@Param("user_id") long user_id);
	
	@Query(value= "Select count(g) from Game g join g.Winner w where w.user.id=:user_id")
	public Long countNbWinByUser(@Param("user_id") long user_id);
}
