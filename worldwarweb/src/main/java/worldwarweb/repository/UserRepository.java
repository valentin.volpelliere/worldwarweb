package worldwarweb.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import worldwarweb.model.Game;
import worldwarweb.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
	public User findByUsername(String username);
	
	public User findByPseudo(String pseudo);
	
	@Query(value="Select g from User u join u.requestgames_receive g where u.id=:id_user" )
	public Set<Game> findAllRequestGames(@Param("id_user")Long id_user);
	
	@Query(value="Select Rf from User u join u.requestfriends_receive Rf where u.id=:id_user" )
	public Set<User> findAllRequestFriends(@Param("id_user")Long id_user);
	
	@Query(value="Select f from User u join u.friends f where u.id=:id_user")
	public Set<User> findAllFriends(@Param("id_user")Long id_user);
	
	@Query(value="SELECT u FROM User u WHERE u.pseudo LIKE %:q% AND u.pseudo!=:pseudo ")
	public Set<User> findTop10ByPseudoLike(@Param("q")String q,@Param("pseudo") String pseudo);
	
	@Query(value="SELECT * FROM \"user\" AS u WHERE id in(Select user_id FROM user_requestgames_receive WHERE requestgames_receive_id=:id_game);", nativeQuery = true)
	public User findUserByRequestGame(@Param("id_game")Long id_game);

}
