package worldwarweb.dto;

import java.io.Serializable;


import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;



@Component
@SessionScope
public class SessionDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private UserDto user=new UserDto();

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}
	
	
	
}
