package worldwarweb.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import worldwarweb.model.User;


public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long userId;
	private Long gameId;
	private Long playerId;
	private String pseudo;

	
	public void setUserDtoByUser(User u) {
		this.pseudo=u.getPseudo();
		this.userId=u.getId();
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}
	
	

	
	
	
	
	

}
