package worldwarweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import worldwarweb.model.User;
import worldwarweb.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	UserRepository userrepository;
	
	public int PseudoAlReadyExist(String pseudo) {
		 User u=userrepository.findByPseudo(pseudo);
		if(u!=null) {
			return 1;
		}
		
		return 0;
	}
	public int AlreadyFriend(List<User> friend,String pseudo) {
		
		 User u=userrepository.findByPseudo(pseudo);

		 if(u != null)
			 for(User temp:friend) {
				 if(u.equals(temp)) {
					 return 1;
				 }
			 }
		 return 0;
	}
	

	
	
}
