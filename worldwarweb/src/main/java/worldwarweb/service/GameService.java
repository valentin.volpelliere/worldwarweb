package worldwarweb.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import java.lang.Math;
import org.springframework.stereotype.Service;

import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.User;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.*;
import worldwarweb.model.unit.Unit;

@Service
public class GameService {
	public static int SELECTED_TILE_ORANGE = 58;
	public static int SELECTED_TILE_RED = 57;
	public static int SELECTED_TILE_GREEN = 56;
	public static int SELECTED_TILE_BLUE = 55;
	
	public static int getWidthCSV(String path) {
		return getFileContents(path).split("\n")[0].split(",").length;
	}
	
	public static int getHeightCSV(String path) {
		return getFileContents(path).split("\n").length;
	}
	
	public static List<Tile> getTileListFromCSV(String path,List<Player> players){
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		String[] fcontentsline = getFileContents(path).split("\n");
		int x = 0;
		int y = 0;
		for(String line : fcontentsline) {
			x = 0;
			String[] words = line.split(",");
			for(String word : words) {
				String[] sub = word.split("\\|");
				String colorstr = sub.length > 1 ? sub[1] : null;
				Player owner = null;
				if(!(colorstr==null) && !(players==null)) {
					for(Player player : players) {
						if(colorstr.toUpperCase().equals(player.getColor().toUpperCase()))
							owner = player;
					}
				}
				switch(sub[0]) {
				case "B":
					tiles.add(new Base(x,y,owner));
					break;
				case "Gate":
					tiles.add(new Gate(x,y));
					break;
				case "V":
					tiles.add(new Village(x,y,owner));
					break;
					
				case "Wa":
					tiles.add(new Water(x,y));
					break;
				case "DWa":
					tiles.add(new DeepWater(x,y));
					break;
				case "Be":
					tiles.add(new Beach(x,y));
					break;
				case "TWa":
					tiles.add(new TropicalWater(x,y));
					break;
				case "SWa":
					tiles.add(new StillWater(x,y));
					break;
				case "TC":
					tiles.add(new TrainingCamp(x,y,owner));
					break;
				case "P":
					tiles.add(new Port(x,y,owner));
					break;
				case "AP":
					tiles.add(new Airport(x,y,owner));
					break;
				case "SA":
					tiles.add(new Sand(x,y));
					break;
				case "SC":
					tiles.add(new CompactSand(x,y));
					break;
				case "CF":
					tiles.add(new CactusFields(x,y));
					break;
					
				case "R":
					tiles.add(new Road(x,y));
					break;
				case "F":
					tiles.add(new Forest(x,y));
					break;
				case "G":
					tiles.add(new Grass(x,y));
					break;
				case "M":
					tiles.add(new Mountain(x,y));
					break;
				case "W":
					tiles.add(new Wall(x,y));
					break;
				default:
					tiles.add(new Beach(x,y));
					System.out.println("UNEXPECTED STRING PARSED IN CSV '" + word +"'" );
					break;
				}
				x++;
			}
			y++;
			
		}
		
		return tiles;
	}
	
	public static void spreadSpeedAlgo(Game game,Unit unit,ArrayList<Tile> accessibleTiles, int x, int y, double remainingMouv) {
		Tile tile = game.getTileAt(x, y);
		if(remainingMouv>=0.) {
			if(accessibleTiles.contains(tile) == false && !(tile instanceof Wall))
				accessibleTiles.add(tile);
			for(int xa=-1;xa<=1;xa++)
				for(int ya=-1;ya<=1;ya++)
					if(xa != ya && xa != -ya) {
						Tile t = game.getTileAt(x+xa,y+ya);
						boolean isEnnemyOn = false;
						if(t!=null)
						{
							for(Player currentPlayer : game.getPlayers())
							{
								for(Unit currentUnit : currentPlayer.getUnits())
								{
									if(currentUnit.getX() == t.getX())
									{
										if(currentUnit.getY() == t.getY())
										{
											if(currentPlayer.getId() != unit.getOwner().getId())
											{
												isEnnemyOn = true;
											}
										}
									}
										
								}
							}
							if(remainingMouv>0. && t.getSpeedCoef(unit) > 0. && isEnnemyOn == false)
								spreadSpeedAlgo(game,
												unit,
												accessibleTiles,
												t.getX(),
												t.getY(),
												remainingMouv-1./t.getSpeedCoef(unit));
						}
					}
		}
	}
	
	public static void spreadAttackAlgo(Game game,Unit unit, ArrayList<Tile> attackableTiles, int x, int y, double remainingRange) {
		Tile tile = game.getTileAt(x, y);
		if(remainingRange >= 0.) {
			
			if(attackableTiles.contains(tile) == false && !(tile instanceof Wall))
				attackableTiles.add(tile);
			
			for(int xa=-1;xa<=1;xa++)
				for(int ya=-1;ya<=1;ya++)
					if(xa != ya && xa != -ya) {
						Tile t = game.getTileAt(x+xa,y+ya);
						if(t != null && remainingRange > 0.) {
							if(t.getRangeObstructCoef(unit) > 0.) {
								spreadAttackAlgo(game,
												unit,
												attackableTiles,
												t.getX(),
												t.getY(),
												remainingRange-1./t.getRangeObstructCoef(unit));
							}
							else {
								if(attackableTiles.contains(t) == false && !(t instanceof Wall))
									attackableTiles.add(t);
							}
						}
						
					}
		}
	}
	
	// Return Start Location of Commander 
	public static int[] getStartLocationCommander(int BaseX,int BaseY,int centerX,int centerY){
		int[] location=new int[2];
		int N,S,E,W=0;
		N=(int) Math.sqrt((Math.pow(BaseX-centerX, 2)+Math.pow(BaseY-1-centerY,2)));
		S=(int) Math.sqrt((Math.pow(BaseX-centerX, 2)+Math.pow(BaseY+1-centerY,2)));
		E=(int) Math.sqrt((Math.pow(BaseX+1-centerX, 2)+Math.pow(BaseY-centerY,2)));
		W=(int) Math.sqrt((Math.pow(BaseX-1-centerX, 2)+Math.pow(BaseY-centerY,2)));
		
		if(E <= W && E <= N && E <=S ) {
			location[0]=BaseX+1;
			location[1]=BaseY;
		}
		else if(W <= E && W <= N && W <= S) {
			location[0]=BaseX-1;
			location[1]=BaseY;
		}
		else if(N <= E && N <= W && N <= S) {
			location[0]=BaseX;
			location[1]=BaseY-1;
		}
		else {
			location[0]=BaseX;
			location[1]=BaseY+1;
		}
		
		
		return location;
	}
	
	public static int GetNbBaseFromMap(String map) {
		List<Tile> Tile =new ArrayList<Tile>();
		int nbBase=0;
		Tile=GameService.getTileListFromCSV("./src/main/resources/static/csv/" + map,null);
		
		for(int i=0;i<Tile.size();i++) {
			//Si la Tile est une Base 
			if(Tile.get(i) instanceof Base) {
				nbBase++;
			}
		}
		return nbBase;
		
	}
	
	

	
	public static String getFileContents(String path) {
		InputStream is;
		String fileAsString = "";
		try {
			is = new FileInputStream(path);
			@SuppressWarnings("resource")
			BufferedReader buf = new BufferedReader(new InputStreamReader(is));
			        
			String line = buf.readLine();
			StringBuilder sb = new StringBuilder();
			        
			while(line != null){
			   sb.append(line).append("\n");
			   line = buf.readLine();
			}
			        
			fileAsString = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileAsString;
	}
	
	//Donne la liste des noms des fichiers se trouvant dans un dossier selon l'extension demandée
		public static List<String> getListFile(String pathDir,String extension){
			File myDir = new File(pathDir);
		 	List<String> listmap=new ArrayList<String>();
		 	for(String str: myDir.list()) {
		 		if(str.contains("."+extension)) {
		 			listmap.add(str);
		 		}
		 	}
	       for (String string : listmap) {
			System.out.println(string);
	       }
	       
	       return listmap;

		}

	
}
