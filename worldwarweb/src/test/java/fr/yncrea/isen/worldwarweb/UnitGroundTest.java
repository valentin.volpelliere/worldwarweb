package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Archer;
import worldwarweb.model.unit.implementations.Commander;
import worldwarweb.model.unit.implementations.Destroyer;
import worldwarweb.model.unit.implementations.Plane;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.model.unit.implementations.Tank;

class UnitGroundTest {//Valou

	@Test
	final void testGetAttackEfficiency() {
		Archer archer =new Archer(null,0,0);
		Destroyer destroyer=new Destroyer(null,0,0);
		Commander commander=new Commander(null, 0, 0);
		Soldier soldier=new Soldier(null,0,0);
		Tank tank=new Tank(null,0,0);

		assertEquals(1.5f, archer.getAttackEfficiency(new Vehicle() {}),0.01f);
		assertEquals(1, archer.getAttackEfficiency(destroyer),0.01f);
		assertEquals(1,destroyer.getAttackEfficiency(new UnitAir()),0.01f);
		assertEquals(1,destroyer.getAttackEfficiency(destroyer),0.01f);
		assertEquals(0, commander.getAttackEfficiency(new UnitAir()),0.01f);
		assertEquals(1.25f, commander.getAttackEfficiency(commander),0.01f);
		assertEquals(0, soldier.getAttackEfficiency(new UnitAir()),0.01f);
		assertEquals(1.25f, soldier.getAttackEfficiency(archer),0.01f);
		assertEquals(1, soldier.getAttackEfficiency(soldier),0.01f);
		assertEquals(1.1f, soldier.getAttackEfficiency(commander),0.01f);
		assertEquals(0, tank.getAttackEfficiency(new UnitAir()),0.01f);
		assertEquals(1.75, tank.getAttackEfficiency(archer),0.01f);
		assertEquals(1, tank.getAttackEfficiency(commander),0.01f);
	}

	@Test
	final void testUnitGround() {
		Archer archer =new Archer();
		Destroyer destroyer=new Destroyer();
		Commander commander=new Commander();
		Soldier soldier=new Soldier();
		Tank tank=new Tank();
		
		assertTrue(archer instanceof UnitGround);
		assertTrue(destroyer instanceof UnitGround);
		assertTrue(commander instanceof UnitGround);
		assertTrue(soldier instanceof UnitGround);
		assertTrue(tank instanceof UnitGround);
	}

	@Test
	final void testUnitGroundPlayerStringIntIntIntIntIntIntIntIntInt() {
		Archer archer =new Archer(null,0,0);
		Destroyer destroyer=new Destroyer(null,0,0);
		Commander commander=new Commander(null, 0, 0);
		Soldier soldier=new Soldier(null,0,0);
		Tank tank=new Tank(null,0,0);
		
		assertTrue(archer instanceof UnitGround);
		assertTrue(destroyer instanceof UnitGround);
		assertTrue(commander instanceof UnitGround);
		assertTrue(soldier instanceof UnitGround);
		assertTrue(tank instanceof UnitGround);
	}

}
