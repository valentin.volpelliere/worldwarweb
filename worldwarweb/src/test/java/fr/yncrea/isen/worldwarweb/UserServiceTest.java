package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import worldwarweb.model.User;
import worldwarweb.repository.UserRepository;
import worldwarweb.service.UserService;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
	
	@Mock
	private UserRepository usersrepository;
	
	@InjectMocks
	private UserService service;
	
	@Test
	final void testPseudoAlReadyExist() {
		User u1=new User();
		User u2=new User();
		Mockito.when(usersrepository.findById(Mockito.anyLong())).thenReturn(Optional.of(u1));
		int verif;
		u2.setId(1L);
		u2.setPseudo("unknown");
		//verif=service.PseudoAlReadyExist(Mockito.times(1)).find);
		//assertEquals(1,verif);
		verif=service.PseudoAlReadyExist(u2.getPseudo());
		assertEquals(0,verif);
	}

	@Test
	final void testAlreadyFriend() {
		User user=new User();
		int verif;
		User friend=new User();
		friend.setPseudo("Valou");
		user.setFriends(new ArrayList<User>());
		user.getFriends().add(friend);
		String alreadyfriend="Valou";
		String nofriend="unknown";
		verif=service.AlreadyFriend(user.getFriends(),alreadyfriend);
		assertEquals(1,verif);
		verif=service.AlreadyFriend(user.getFriends(),nofriend);
		assertEquals(0,verif);
		
	}

}
