package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.Airport;
import worldwarweb.model.tile.implementations.Base;
import worldwarweb.model.tile.implementations.Beach;
import worldwarweb.model.tile.implementations.CactusFields;
import worldwarweb.model.tile.implementations.CompactSand;
import worldwarweb.model.tile.implementations.DeepWater;
import worldwarweb.model.tile.implementations.Forest;
import worldwarweb.model.tile.implementations.Gate;
import worldwarweb.model.tile.implementations.Grass;
import worldwarweb.model.tile.implementations.Mountain;
import worldwarweb.model.tile.implementations.Port;
import worldwarweb.model.tile.implementations.Road;
import worldwarweb.model.tile.implementations.Sand;
import worldwarweb.model.tile.implementations.StillWater;
import worldwarweb.model.tile.implementations.TrainingCamp;
import worldwarweb.model.tile.implementations.TropicalWater;
import worldwarweb.model.tile.implementations.Village;
import worldwarweb.model.tile.implementations.Wall;
import worldwarweb.model.tile.implementations.Water;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitGround;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Car;
import worldwarweb.model.unit.implementations.Commander;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.model.unit.implementations.Submarine;
import worldwarweb.model.unit.implementations.Tank;
import worldwarweb.service.GameService;

class TileTest {

	@Test
	final void testToDataHTML() {	//Kyll
		 Village v = new Village(0, 0, null);
		 assertEquals("122,NULL,0,100",v.toDataHTML());
	}

	@Test
	final void testGetSpeedCoef() {	//Valou
		Road road =new Road();
		DeepWater deepWater=new DeepWater();
		StillWater stillWater= new StillWater();
		
		
		assertEquals(1, road.getSpeedCoef(new UnitAir()));
		assertEquals(0, road.getSpeedCoef(new UnitWater() {}));
		assertEquals(1.25, road.getSpeedCoef(new UnitGround() {}));
		
		assertEquals(1, deepWater.getSpeedCoef(new UnitAir()));
		assertEquals(1.25, deepWater.getSpeedCoef(new UnitWater() {}));
		assertEquals(0, deepWater.getSpeedCoef(new UnitGround() {}));
		
		assertEquals(1, stillWater.getSpeedCoef(new UnitAir()));
		assertEquals(1.25, stillWater.getSpeedCoef(new UnitWater() {}));
		assertEquals(0, stillWater.getSpeedCoef(new UnitGround() {}));
	}

	@Test
	final void testGetDamageCoef() { //Chris
		String path = "src/main/resources/static/csv/map_test_allTiles.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(null,0,0);
		Commander commander = new Commander(null,0,0);
		Boat boat = new Boat(null,0,0);
		Barge barge = new Barge(null,0,0);
		Boat plane = new Boat(null,0,0);
		Car car = new Car(null,0,0);
		Submarine submarine = new Submarine(null,0,0);
		BattleShip battleship = new BattleShip(null,0,0);
		Tank tank = new Tank(null,0,0);
		
		assertEquals(1, game.getTileAt(5, 0).getDamageCoef(soldier)); //grass
		assertEquals(1, game.getTileAt(5, 1).getDamageCoef(soldier)); //wall
		assertEquals(1.5f, game.getTileAt(5, 2).getDamageCoef(soldier)); //forest
		assertEquals(1.5f, game.getTileAt(5, 2).getDamageCoef(commander)); //forest
		assertEquals(1, game.getTileAt(5, 2).getDamageCoef(boat)); //forest
		assertEquals(1, game.getTileAt(5, 2).getDamageCoef(plane)); //forest
		assertEquals(1, game.getTileAt(5, 3).getDamageCoef(soldier)); //mountain
		assertEquals(1, game.getTileAt(5, 4).getDamageCoef(soldier)); //village
		assertEquals(1, game.getTileAt(5, 5).getDamageCoef(soldier)); //base
		assertEquals(0, game.getTileAt(5, 6).getDamageCoef(soldier)); //gate
		assertEquals(1, game.getTileAt(5, 7).getDamageCoef(soldier)); //road
		assertEquals(1, game.getTileAt(5, 8).getDamageCoef(soldier)); //water
		
		assertEquals(1, game.getTileAt(5, 9).getDamageCoef(soldier)); //deep water
		assertEquals(1, game.getTileAt(5, 9).getDamageCoef(car)); //deep water
		assertEquals(1, game.getTileAt(5, 9).getDamageCoef(barge)); //deep water
		assertEquals(1, game.getTileAt(5, 9).getDamageCoef(plane)); //deep water
		assertEquals(1.75f, game.getTileAt(5, 9).getDamageCoef(submarine)); //deep water
		
		assertEquals(1, game.getTileAt(5, 10).getDamageCoef(soldier)); //beach
		assertEquals(1.5f, game.getTileAt(5, 10).getDamageCoef(battleship)); //beach
		
		assertEquals(1, game.getTileAt(5, 11).getDamageCoef(soldier)); //tropical water
		
		assertEquals(1, game.getTileAt(5, 12).getDamageCoef(soldier)); //still water
		assertEquals(1.25f, game.getTileAt(5, 12).getDamageCoef(boat)); //still water
		
		assertEquals(1, game.getTileAt(5, 13).getDamageCoef(soldier)); //training camp
		assertEquals(1, game.getTileAt(5, 14).getDamageCoef(soldier)); //port
		
		assertEquals(1, game.getTileAt(5, 15).getDamageCoef(soldier)); //sand
		assertEquals(1.5f, game.getTileAt(5, 15).getDamageCoef(tank)); //sand
		
		assertEquals(1, game.getTileAt(5, 16).getDamageCoef(soldier)); //sand compact
		assertEquals(1, game.getTileAt(5, 17).getDamageCoef(soldier)); //cactus field
		
	}

	@Test
	final void testGetRangeObstructCoef() { //Kyll
		Grass grass = new Grass(0,0);
		Mountain mountain = new Mountain(0,0);
		
		assertEquals(1, grass.getRangeObstructCoef(new Soldier(null,0,0)),0.001);
		assertEquals(0.5f, mountain.getRangeObstructCoef(new Soldier(null,0,0)),0.001);
	}

	@Test
	final void testGetRangeBonusCoef() { //Valou
		Sand sand =new Sand();
		Beach beach=new Beach();

		assertEquals(1,sand.getRangeBonusCoef(new UnitAir()));
		assertEquals(1, sand.getRangeBonusCoef(new UnitWater() {}));
		assertEquals(1, sand.getRangeBonusCoef(new UnitGround() {}));
		
		assertEquals(1,beach.getRangeBonusCoef(new UnitAir()));
		assertEquals(1.5, beach.getRangeBonusCoef(new UnitWater() {}));
		assertEquals(1, beach.getRangeBonusCoef(new UnitGround() {}));
		
	}

	@Test
	final void testGetDefenseCoef() { //Kyll
		Grass grass = new Grass(0,0);
		Mountain mountain = new Mountain(0,0);
		
		assertEquals(1, grass.getRangeObstructCoef(new Soldier(null,0,0)),0.001);
		assertEquals(0.5, mountain.getRangeObstructCoef(new Soldier(null,0,0)),0.001);
	}
	
	@Test
	final void testTileStringIntIntInt() { //Chris
		Airport airport = new Airport(0,0,null);
		Base base = new Base(0,0,null);
		Beach beach = new Beach(0,0);
		CactusFields cactusfields = new CactusFields(0, 0);
		CompactSand compactsand = new CompactSand(0,0);
		DeepWater deepwater = new DeepWater(0,0);
		Forest forest = new Forest(0,0);
		Gate gate = new Gate(0,0);
		Grass grass = new Grass(0,0);
		Mountain mountain = new Mountain(0,0);
		Port port = new Port(0,0,null);
		Road road = new Road(0,0);
		Sand sand = new Sand(0,0);
		StillWater stillwater = new StillWater(0,0);
		TrainingCamp trainingCamp = new TrainingCamp(0, 0, null);
		TropicalWater tropicalwater = new TropicalWater(0, 0);
		Village village = new Village(0,0,null);
		Wall wall = new Wall(0, 0);
		Water water = new Water(0,0);
		
		assertTrue(airport instanceof Tile);
		assertTrue(base instanceof Tile);
		assertTrue(beach instanceof Tile);
		assertTrue(cactusfields instanceof Tile);
		assertTrue(compactsand instanceof Tile);
		assertTrue(deepwater instanceof Tile);
		assertTrue(forest instanceof Tile);
		assertTrue(gate instanceof Tile);
		assertTrue(grass instanceof Tile);
		assertTrue(mountain instanceof Tile);
		assertTrue(port instanceof Tile);
		assertTrue(road instanceof Tile);
		assertTrue(sand instanceof Tile);
		assertTrue(stillwater instanceof Tile);
		assertTrue(trainingCamp instanceof Tile);
		assertTrue(tropicalwater instanceof Tile);
		assertTrue(village instanceof Tile);
		assertTrue(wall instanceof Tile);
		assertTrue(water instanceof Tile);
	}

}
