package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Plane;
import worldwarweb.model.unit.implementations.Submarine;

class UnitWaterTest {

	@Test
	final void testGetAttackEfficiency() {
		Barge barge = new Barge(null,0,0);
		Boat boat = new Boat(null,0,0);
		BattleShip battleship = new BattleShip(null,0,0);
		Submarine submarine = new Submarine(null,0,0);
		
		assertEquals(0       , barge     .getAttackEfficiency(boat       ),0.01f);
		assertEquals(1       , boat      .getAttackEfficiency(boat       ),0.01f);
		assertEquals(1.5f   , submarine .getAttackEfficiency(boat       ),0.01f);
		assertEquals(1.5f    , battleship.getAttackEfficiency(new Plane()),0.01f);
	}

	@Test
	final void testUnitWater() {
		Boat boat = new Boat();
		BattleShip battleship = new BattleShip();
		Submarine submarine = new Submarine();
		
		assertTrue(boat       instanceof UnitWater);
		assertTrue(battleship instanceof UnitWater);
		assertTrue(submarine  instanceof UnitWater);
	}

	@Test
	final void testUnitWaterPlayerStringIntIntIntIntIntIntIntIntInt() {
		Boat boat = new Boat(null,0,0);
		BattleShip battleship = new BattleShip(null,0,0);
		Submarine submarine = new Submarine(null,0,0);
		
		assertTrue(boat       instanceof UnitWater);
		assertTrue(battleship instanceof UnitWater);
		assertTrue(submarine  instanceof UnitWater);
	}

}
