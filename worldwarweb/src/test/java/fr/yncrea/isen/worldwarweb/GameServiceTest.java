package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.Grass;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.service.GameService;

@ExtendWith(MockitoExtension.class)
class GameServiceTest {
	
	
	@Test
	final void testGetWidthCSV() {
		int widthmap1 = 20;
		int widthmap2 = 40;
		
		int testwidthmap1 = GameService.getWidthCSV("./src/main/resources/static/csv/map1.csv");
		int testwidthmap2 = GameService.getWidthCSV("./src/main/resources/static/csv/map2.csv");
		
		assertEquals(widthmap1, testwidthmap1);
		assertEquals(widthmap2, testwidthmap2);
	}

	@Test
	final void testGetHeightCSV() {
		int heightmap1 = 21;
		int heightmap2 = 30;
		
		int testheightmap1 = GameService.getHeightCSV("./src/main/resources/static/csv/map1.csv");
		int testheightmap2 = GameService.getHeightCSV("./src/main/resources/static/csv/map2.csv");
		
		assertEquals(heightmap1, testheightmap1);
		assertEquals(heightmap2, testheightmap2);
	}

	@Test
	final void testGetTileListFromCSV() {
		 GameService.getTileListFromCSV("./src/main/resources/static/csv/map demo.csv", new ArrayList<Player>());
		List<Tile> tiles = GameService.getTileListFromCSV("./src/main/resources/static/csv/minimap.csv", new ArrayList<Player>());
		assertEquals(tiles.size(), 4);
		for(Tile tile : tiles)
			assertTrue(tile instanceof Grass);
	}

	@Test
	final void testSpreadSpeedAlgo() {
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = soldier.getAccessibleTiles();
		ArrayList<Tile> must_Tiles = new ArrayList<Tile>();
		must_Tiles.add(game.getTileAt(7, 5));
		must_Tiles.add(game.getTileAt(6,5));
		must_Tiles.add(game.getTileAt(5,5));
		must_Tiles.add(game.getTileAt(4,5));
		must_Tiles.add(game.getTileAt(5,6));
		must_Tiles.add(game.getTileAt(5,4));
		must_Tiles.add(game.getTileAt(7,4));
		must_Tiles.add(game.getTileAt(7,3));
		must_Tiles.add(game.getTileAt(7,2));
		must_Tiles.add(game.getTileAt(7,6));
		must_Tiles.add(game.getTileAt(7,7));
		must_Tiles.add(game.getTileAt(8,5));
		must_Tiles.add(game.getTileAt(8,4));
		must_Tiles.add(game.getTileAt(8,3));
		must_Tiles.add(game.getTileAt(8,6));
		must_Tiles.add(game.getTileAt(9,4));
		must_Tiles.add(game.getTileAt(9,5));
		must_Tiles.add(game.getTileAt(9,6));
		must_Tiles.add(game.getTileAt(10,5));
		for(Tile tile : must_Tiles)
			assertEquals(true, ret_Tiles.contains(tile));
	}

	@Test
	final void testSpreadAttackAlgo() {
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = new ArrayList<Tile>();
		soldier.getAttackableTiles(ret_Tiles, soldier.getX(), soldier.getY());
		
		ArrayList<Tile> must_Tiles = new ArrayList<Tile>();
		must_Tiles.add(game.getTileAt(7, 5));
		must_Tiles.add(game.getTileAt(6,5));
		must_Tiles.add(game.getTileAt(8,5));
		must_Tiles.add(game.getTileAt(7,4));
		for(Tile tile : must_Tiles)
			assertEquals(true, ret_Tiles.contains(tile));
	}

	@Test
	final void testGetStartLocationCommander() {
		int BaseX=50;
		int BaseY=50;
		int CentreX=100;
		int CentreY=50;
		int[] location=new int[2];
		
		location=GameService.getStartLocationCommander(BaseX, BaseY, CentreX, CentreY);
		assertEquals(BaseX+1,location[0]);
		assertEquals(BaseY,location[1]);
		CentreX=0;
		location=GameService.getStartLocationCommander(BaseX, BaseY, CentreX, CentreY);
		assertEquals(BaseX-1,location[0]);
		assertEquals(BaseY,location[1]);
		CentreX=50;
		CentreY=100;
		location=GameService.getStartLocationCommander(BaseX, BaseY, CentreX, CentreY);
		assertEquals(BaseX,location[0]);
		assertEquals(BaseY+1,location[1]);
		CentreY=0;
		location=GameService.getStartLocationCommander(BaseX, BaseY, CentreX, CentreY);
		assertEquals(BaseX,location[0]);
		assertEquals(BaseY-1,location[1]);
	}

	@Test
	final void testGetFileContents() {
		String map1 = "V,G,G,G,G,G,M,M,G,V,G,G,M,M,G,G,G,G,G,V\n" + 
				"G,G,G,G,G,M,M,G,G,G,G,G,G,M,M,G,G,G,G,G\n" + 
				"W,W,W,W,W,W,W,G,G,G,G,G,G,W,W,W,W,W,W,W\n" + 
				"G,G,G,G,V,G,W,G,G,G,G,G,G,W,G,V,G,G,G,G\n" + 
				"G,G,F,F,G,G,W,G,G,G,G,G,G,W,G,G,F,F,G,G\n" + 
				"G,F,F,G,R,R,R,R,R,R,R,R,R,R,R,R,G,F,F,G\n" + 
				"G,G,G,G,R,G,W,F,F,G,G,G,G,W,G,R,G,G,G,G\n" + 
				"M,M,G,G,R,M,W,G,F,G,G,G,G,W,M,R,G,G,M,M\n" + 
				"M,G,G,G,R,G,W,G,F,F,F,G,G,W,G,R,G,G,G,M\n" + 
				"G,G,G,F,R,G,W,G,G,F,F,G,G,W,G,R,F,G,G,G\n" + 
				"B|Red,R,R,TC|red,R,G,W,G,G,F,V,G,G,W,G,R,V|blue,R,R,TC|blue\n" + 
				"G,G,G,F,R,G,W,G,G,V,F,G,G,W,G,R,F,G,G,G\n" + 
				"M,G,G,G,R,G,W,G,G,F,F,G,G,W,M,R,G,G,G,M\n" + 
				"M,M,G,G,R,M,W,G,G,F,F,F,G,W,G,R,G,G,M,M\n" + 
				"G,G,G,G,R,G,W,G,G,G,G,F,F,W,G,R,G,G,G,G\n" + 
				"G,F,F,G,R,R,R,R,R,R,R,R,R,R,R,R,G,F,F,G\n" + 
				"G,G,F,F,G,G,W,G,G,G,G,G,G,W,G,G,F,F,G,G\n" + 
				"G,G,G,G,V,G,W,G,G,G,G,G,G,W,G,V,G,G,G,G\n" + 
				"W,W,W,W,W,W,W,G,G,G,G,G,G,W,W,W,W,W,W,W\n" + 
				"G,G,G,G,G,M,M,G,G,G,G,G,G,M,M,G,G,G,G,G\n" + 
				"V,G,G,G,G,G,M,M,G,G,V,G,M,M,G,G,G,G,G,V\n";
		String map2 = "G,G,G,G,G,G,V|red,W,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,R,W,G,G,G,G,G,G,G,G,G,V,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,R,W,G,G,G,G,G,G,G,G,G,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,B|red,R,R,R,R,R,R,R,R,R,R,R,R,R,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,TC|red,G,W,G,G,G,M,M,M,M,M,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,F,G,G,G,W,G,G,G,M,M,M,M,M,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"W,W,F,F,F,W,W,W,G,G,G,G,G,M,G,G,R,F,G,G,G,SA,SA,SA,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,F,F,F,F,F,F,G,G,G,G,G,G,V,G,G,R,F,G,G,SA,SA,SA,SA,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,F,F,F,F,F,F,F,G,G,G,G,G,R,G,G,R,F,F,F,SA,SA,SC,SC,SC,SA,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,F,F,F,F,F,F,G,G,G,G,G,R,TWa,TWa,R,F,F,F,SA,SA,SC,SC,SC,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,F,F,F,G,G,G,G,G,G,R,TWa,G,R,F,G,G,SA,SA,SA,SC,SC,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,F,F,F,G,G,V,G,G,G,R,G,G,R,G,G,G,G,SA,SA,SC,SC,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,F,G,G,G,R,R,R,R,R,R,R,R,G,G,G,G,SA,SA,SC,SA,SA,SA,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,R,G,G,G,G,G,G,R,G,G,G,G,SA,SA,SA,SA,CF,CF,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,R,G,Wa,Wa,Wa,G,G,R,G,G,G,G,G,G,G,G,SA,CF,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,R,G,Wa,Wa,Wa,Wa,Wa,R,G,G,G,G,G,G,G,G,G,SA,G,G,G,G,G,G,G,G,G,G,V,G,G\n" + 
				"G,G,G,G,R,R,R,R,R,R,G,G,G,Wa,DWa,Wa,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,R,G,G,G,G,R,R,R,R,Wa,DWa,Wa,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,F,G,G,G,G\n" + 
				"V,R,R,R,R,G,G,G,G,G,G,G,R,Wa,Wa,Wa,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,F,F,F,G,G,G\n" + 
				"G,G,G,G,R,G,G,G,G,G,G,G,R,G,G,G,R,G,G,G,G,G,G,G,G,G,G,G,G,V,G,G,F,F,F,F,F,F,G,G\n" + 
				"G,G,G,G,R,G,SWa,SWa,SWa,G,G,G,R,R,R,R,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,F,F,F,F,F,F,G\n" + 
				"G,G,G,G,R,G,SWa,M,M,M,G,G,R,G,G,G,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,F,F,F,F,F,G\n" + 
				"G,G,G,G,R,G,SWa,SWa,M,M,G,G,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,W,W,W,W,W,W,W\n" + 
				"G,G,G,G,R,R,R,R,R,R,R,R,R,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,W,G,G,G,F,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,W,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,V,G,G,G,G,G,G,W,G,G,B|blue,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,W,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,W,G,G,G,G,G,G\n" + 
				"G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,G,V,G,G,G,G,G,G,G,G,G,G,G,W,G,G,G,G,G,G\n";
		
		String testmap1 = GameService.getFileContents("./src/main/resources/static/csv/map1.csv");
		String testmap2 = GameService.getFileContents("./src/main/resources/static/csv/map2.csv");
		
		assertEquals(map1, testmap1);
		assertEquals(map2, testmap2);
	}

	@Test
	final void testGetListFile() {
		List<String> list = GameService.getListFile("./src/main/resources/static/csv/", "csv");
		assertTrue(list.contains("map1.csv"));
		assertTrue(list.contains("map2.csv"));
		assertTrue(list.contains("minimap.csv"));
	}
	
	
	@Test
	final void testGetNbBaseFromMap() {
		String namemap="Dragon Creek.csv";
		assertEquals(2, GameService.GetNbBaseFromMap(namemap));
	}
	

}
