package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import worldwarweb.model.Game;
import worldwarweb.model.Player;
import worldwarweb.model.tile.Building;
import worldwarweb.model.tile.Tile;
import worldwarweb.model.tile.implementations.Base;
import worldwarweb.model.tile.implementations.Port;
import worldwarweb.model.tile.implementations.TrainingCamp;
import worldwarweb.model.tile.implementations.Village;
import worldwarweb.model.unit.Unit;
import worldwarweb.model.unit.Vehicle;
import worldwarweb.model.unit.implementations.Archer;
import worldwarweb.model.unit.implementations.Barge;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Car;
import worldwarweb.model.unit.implementations.Commander;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.model.unit.implementations.StealthPlane;
import worldwarweb.model.unit.implementations.Submarine;
import worldwarweb.service.GameService;

class UnitTest {

	@Test
	final void testUnit() {	//Valou
		Unit unit=new Unit(new Player(),"Soldier",26,0,0,0,0,100,150,100,1) {
			
			@Override
			public float getAttackEfficiency(Unit enemy) {
				return 0;
			}
		};
		
		assertTrue(unit.getOwner() instanceof Player);
		assertEquals("Soldier",unit.getName());
		assertEquals(26,unit.getTextureid());
		assertEquals(0, unit.getX());
		assertEquals(0,unit.getY());
		assertEquals(0, unit.getRange_mvmt());
		assertEquals(0, unit.getRange_atk());
		assertEquals(100,unit.getHp());
		assertEquals(150,unit.getAtk());
		assertEquals(100,unit.getCost());
		assertEquals(1, unit.getMax_attack_per_round());
		assertEquals(null,unit.getPassenger());
		assertEquals(0,unit.getNb_attack_round());
		assertEquals(0, unit.getAttackEfficiency(unit));
		
	}

	@Test
	final void testUnitPlayerStringIntIntIntIntIntIntIntIntInt() {	//Kyll
		Soldier soldier = new Soldier(null,0,0);
		StealthPlane stealth = new StealthPlane(null,0,0);
		Boat boat = new Boat(null,0,0);
		BattleShip battleship = new BattleShip(null,0,0);
		Submarine submarine = new Submarine(null,0,0);
		
		assertTrue(soldier    instanceof Unit);
		assertTrue(stealth    instanceof Unit);
		assertTrue(boat       instanceof Unit);
		assertTrue(battleship instanceof Unit);
		assertTrue(submarine  instanceof Unit);
	}

	@Test
	final void testToDataHTML() {	//Kyll
		Soldier soldier = new Soldier(null,0,0);
		StealthPlane stealth = new StealthPlane(null,0,0);
		Boat boat = new Boat(null,0,0);
		
		stealth.setHp((int)(boat.getHpmax()*0.59));
		boat.setHp((int)(boat.getHpmax()*0.01));
		
		assertEquals("59,100",soldier.toDataHTML());
		assertEquals("76,59",stealth.toDataHTML());
		assertEquals("79,1",boat.toDataHTML());
	}

	@Test
	final void testPafFauteuilleRoulant() {	//Chris
		Soldier soldier = new Soldier(null,0,0);
		assertEquals(3, soldier.getRange_mvmt());
		assertEquals(3, soldier.getRange_mvmt_max());
		soldier.pafFauteuilleRoulant();
		assertEquals(0, soldier.getRange_mvmt());
		assertEquals(3, soldier.getRange_mvmt_max());
	}

	@Test
	final void testIsVehicle() {	//Kyll
		Soldier soldier = new Soldier(null,0,0);
		StealthPlane stealth = new StealthPlane(null,0,0);
		Car car = new Car(null,0,0);
		Barge barge = new Barge(null,0,0);
		
		assertEquals(false, soldier.isVehicle());
		assertEquals(false, stealth.isVehicle());
		assertEquals(true, car.isVehicle());
		assertEquals(true, barge.isVehicle());
	}

	@Test
	final void testGetPassengerList() { //Kyll
		StealthPlane stealth = new StealthPlane(null,0,0);
		Car car = new Car(null,0,0);
		Barge barge = new Barge(null,0,0);
		
		car.load(new Archer());
		barge.load(new Commander());
		barge.load(new Soldier());
		
		assertEquals(null, stealth.getPassengerList());
		assertEquals(1,car.getPassengerList().size());
		assertEquals(2,barge.getPassengerList().size());
		assertTrue(car.getPassengerList().get(0) instanceof Archer);
	}

	@Test
	final void testGetAccessibleTiles() { //Chris
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = soldier.getAccessibleTiles();
		ArrayList<Tile> must_Tiles = new ArrayList<Tile>();
		must_Tiles.add(game.getTileAt(7, 5));
		must_Tiles.add(game.getTileAt(6,5));
		must_Tiles.add(game.getTileAt(5,5));
		must_Tiles.add(game.getTileAt(4,5));
		must_Tiles.add(game.getTileAt(5,6));
		must_Tiles.add(game.getTileAt(5,4));
		must_Tiles.add(game.getTileAt(7,4));
		must_Tiles.add(game.getTileAt(7,3));
		must_Tiles.add(game.getTileAt(7,2));
		must_Tiles.add(game.getTileAt(7,6));
		must_Tiles.add(game.getTileAt(7,7));
		must_Tiles.add(game.getTileAt(8,5));
		must_Tiles.add(game.getTileAt(8,4));
		must_Tiles.add(game.getTileAt(8,3));
		must_Tiles.add(game.getTileAt(8,6));
		must_Tiles.add(game.getTileAt(9,4));
		must_Tiles.add(game.getTileAt(9,5));
		must_Tiles.add(game.getTileAt(9,6));
		must_Tiles.add(game.getTileAt(10,5));
		for(Tile tile : must_Tiles)
			assertEquals(true, ret_Tiles.contains(tile));
	}

	@Test
	final void testGetAttackableTiles() { //Chris
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = new ArrayList<Tile>();
		soldier.getAttackableTiles(ret_Tiles, soldier.getX(), soldier.getY());
		
		ArrayList<Tile> must_Tiles = new ArrayList<Tile>();
		must_Tiles.add(game.getTileAt(7, 5));
		must_Tiles.add(game.getTileAt(6,5));
		must_Tiles.add(game.getTileAt(8,5));
		must_Tiles.add(game.getTileAt(7,4));
		for(Tile tile : must_Tiles)
			assertEquals(true, ret_Tiles.contains(tile));
	}

	@Test
	final void testGetTilePosition() { //Valou
		List<Tile> grid=new ArrayList<Tile>();
		Tile tile=new Tile("Grass",26,0,0) {
			
			@Override
			public float getSpeedCoef(Unit unit) {
				return 0;
			}
			
			@Override
			public float getRangeObstructCoef(Unit unit) {
				return 0;
			}
			
			@Override
			public float getRangeBonusCoef(Unit unit) {

				return 0;
			}
			
			@Override
			public float getDefenseCoef(Unit unit) {
				return 0;
			}
			
			@Override
			public float getDamageCoef(Unit unit) {

				return 0;
			}
		};
		grid.add(tile);
		Game g=new Game(grid,new ArrayList<Player>(),50,50);
		Player p=new Player();
		Unit unit =new Unit(p,"Soldier",26,0,0,0,0,100,150,100,1) {
			@Override
			public float getAttackEfficiency(Unit enemy) {
				return 0;
			}
		};
		p.setGame(g);
		Tile newtile = unit.getTilePosition();
		
		assertTrue(newtile instanceof Tile);
		assertEquals(0, newtile.getSpeedCoef(unit));
		assertEquals(0, newtile.getRangeBonusCoef(unit));
		assertEquals(0, newtile.getRangeObstructCoef(unit));
		assertEquals(0, newtile.getDefenseCoef(unit));
		assertEquals(0, newtile.getDamageCoef(unit));
	}

	@Test
	final void testGetAllAttackableTiles() { //Chris
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = soldier.getAllAttackableTiles(soldier.getAccessibleTiles());
		ArrayList<Tile> must_Tiles = new ArrayList<Tile>();
		must_Tiles.add(game.getTileAt(7, 5));
		must_Tiles.add(game.getTileAt(6,5));
		must_Tiles.add(game.getTileAt(5,5));
		must_Tiles.add(game.getTileAt(4,5));
		must_Tiles.add(game.getTileAt(5,6));
		must_Tiles.add(game.getTileAt(5,4));
		must_Tiles.add(game.getTileAt(7,4));
		must_Tiles.add(game.getTileAt(7,3));
		must_Tiles.add(game.getTileAt(7,2));
		must_Tiles.add(game.getTileAt(7,6));
		must_Tiles.add(game.getTileAt(7,7));
		must_Tiles.add(game.getTileAt(8,5));
		must_Tiles.add(game.getTileAt(8,4));
		must_Tiles.add(game.getTileAt(8,3));
		must_Tiles.add(game.getTileAt(8,6));
		must_Tiles.add(game.getTileAt(9,4));
		must_Tiles.add(game.getTileAt(9,5));
		must_Tiles.add(game.getTileAt(9,6));
		must_Tiles.add(game.getTileAt(10,5));
		
		must_Tiles.add(game.getTileAt(3,5));
		must_Tiles.add(game.getTileAt(4,4));
		must_Tiles.add(game.getTileAt(5,3));
		must_Tiles.add(game.getTileAt(7,1));
		must_Tiles.add(game.getTileAt(8,2));
		must_Tiles.add(game.getTileAt(9,3));
		must_Tiles.add(game.getTileAt(10,4));
		must_Tiles.add(game.getTileAt(11,5));
		must_Tiles.add(game.getTileAt(10,6));
		must_Tiles.add(game.getTileAt(9,7));
		must_Tiles.add(game.getTileAt(8,7));
		must_Tiles.add(game.getTileAt(7,8));
		must_Tiles.add(game.getTileAt(5,7));
		must_Tiles.add(game.getTileAt(4,6));
		
		for(Tile tile : must_Tiles)
			assertEquals(true, ret_Tiles.contains(tile));
	}

	@Test
	final void testHeal() {	//Chris
		Soldier soldier = new Soldier(null,0,0);
		soldier.setHp(150);
		soldier.heal(50);
		assertEquals(soldier.getHpmax(), soldier.getHp());
	}

	@Test
	final void testCanAttackTile() { //Chris
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		ArrayList<Tile> ret_Tiles = soldier.getAllAttackableTiles(soldier.getAccessibleTiles());
		
		assertEquals(true, soldier.canAttackTile(game.getTileAt(8, 4)));
		assertEquals(false, soldier.canAttackTile(game.getTileAt(8, 8)));
	}

	@Test
	final void testSetTileSelectionBox() {
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		Soldier soldier = new Soldier(player1,7,5);
		player1.getUnits().add(soldier);
		
		soldier.setTileSelectionBox();
		
		ArrayList<Tile> must_accessible_Tiles = new ArrayList<Tile>();
		must_accessible_Tiles.add(game.getTileAt(7, 5));
		must_accessible_Tiles.add(game.getTileAt(6,5));
		must_accessible_Tiles.add(game.getTileAt(5,5));
		must_accessible_Tiles.add(game.getTileAt(4,5));
		must_accessible_Tiles.add(game.getTileAt(5,6));
		must_accessible_Tiles.add(game.getTileAt(5,4));
		must_accessible_Tiles.add(game.getTileAt(7,4));
		must_accessible_Tiles.add(game.getTileAt(7,3));
		must_accessible_Tiles.add(game.getTileAt(7,2));
		must_accessible_Tiles.add(game.getTileAt(7,6));
		must_accessible_Tiles.add(game.getTileAt(7,7));
		must_accessible_Tiles.add(game.getTileAt(8,5));
		must_accessible_Tiles.add(game.getTileAt(8,4));
		must_accessible_Tiles.add(game.getTileAt(8,3));
		must_accessible_Tiles.add(game.getTileAt(8,6));
		must_accessible_Tiles.add(game.getTileAt(9,4));
		must_accessible_Tiles.add(game.getTileAt(9,5));
		must_accessible_Tiles.add(game.getTileAt(9,6));
		must_accessible_Tiles.add(game.getTileAt(10,5));
		
		ArrayList<Tile> must_attackable_Tiles = new ArrayList<Tile>();
		must_attackable_Tiles.add(game.getTileAt(3,5));
		must_attackable_Tiles.add(game.getTileAt(4,4));
		must_attackable_Tiles.add(game.getTileAt(5,3));
		must_attackable_Tiles.add(game.getTileAt(7,1));
		must_attackable_Tiles.add(game.getTileAt(8,2));
		must_attackable_Tiles.add(game.getTileAt(9,3));
		must_attackable_Tiles.add(game.getTileAt(10,4));
		must_attackable_Tiles.add(game.getTileAt(11,5));
		must_attackable_Tiles.add(game.getTileAt(10,6));
		must_attackable_Tiles.add(game.getTileAt(9,7));
		must_attackable_Tiles.add(game.getTileAt(8,7));
		must_attackable_Tiles.add(game.getTileAt(7,8));
		must_attackable_Tiles.add(game.getTileAt(5,7));
		must_attackable_Tiles.add(game.getTileAt(4,6));
		
		for(Tile t : must_accessible_Tiles) {
			assertEquals(GameService.SELECTED_TILE_ORANGE, t.getSelect_tex_id());
		}
		for(Tile t : must_attackable_Tiles) {
			assertEquals(GameService.SELECTED_TILE_RED, t.getSelect_tex_id());
		}
	}

	@Test
	final void testCalculateAttackDamageUnit() { //Kyll
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		
		// -- Test Attackable Cases, Movable Cases --

		//Create test units
		Unit[] units = new Unit[]{
			new Commander(player1, 4, 3),
			new Soldier(player1, 11,11),
		};
		
		for(Unit u : units) {
			player1.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		Unit[] units2 = new Unit[]{
			new Soldier(player2, 3,3),
			new Archer(player2,9,5)
		};
		
		for(Unit u : units2) {
			player2.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		
		
		assertEquals(7, units2[0].calculateAttackDamage(units[0]));
		assertEquals(300, units[0].calculateAttackDamage(units2[0]));
	}

	@Test
	final void testAttackUnit() { 	//Kyll
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		
		// -- Test Attackable Cases, Movable Cases --

		//Create test units
		Unit[] units = new Unit[]{
			new Commander(player1, 4, 3),
			new Soldier(player1, 11,11),
		};
		
		for(Unit u : units) {
			player1.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		Unit[] units2 = new Unit[]{
			new Soldier(player2, 3,3),
			new Archer(player2,9,5)
		};
		
		for(Unit u : units2) {
			player2.getUnits().add(u);
			u.setTileSelectionBox();
		}
		units2[0].attack(units[0]);
		
		
		assertEquals(0, units2[0].getHp());
		assertEquals(793, units[0].getHp());
	}

	@Test
	final void testCounterAttack() { //Kyll
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		
		Unit[] units = new Unit[]{
			new Commander(player1, 4, 3),
			new Soldier(player1, 11,11),
		};
		
		for(Unit u : units) {
			player1.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		Unit[] units2 = new Unit[]{
			new Soldier(player2, 3,3),
			new Archer(player2,9,5)
		};
		
		for(Unit u : units2) {
			player2.getUnits().add(u);
			u.setTileSelectionBox();
		}
		units2[0].counterAttack(units[0]);
		
		
		assertEquals(200, units2[0].getHp());
		assertEquals(795, units[0].getHp());
	}

	@Test
	final void testCalculateAttackDamageBuilding() { //Kyll
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		
		// -- Test Attackable Cases, Movable Cases --

		//Create test units
		Unit[] units = new Unit[]{
			new Commander(player1, 4, 3),
			new Soldier(player1, 11,11),
		};
		
		for(Unit u : units) {
			player1.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		Unit[] units2 = new Unit[]{
			new Soldier(player2, 1,10),
			new Archer(player2,9,5)
		};
		
		for(Unit u : units2) {
			player2.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		assertEquals(80,units2[0].calculateAttackDamage((Building)game.getTileAt(0, 10)));
	}

	@Test
	final void testAttackBuilding() { //Kyl
		String path = "src/main/resources/static/csv/map1.csv";
		ArrayList<Player> players = new ArrayList<>();
		Player player1 = new Player("nom", new ArrayList<Unit>(), null, 100, "red",null, null);
		Player player2 = new Player("nom2", new ArrayList<Unit>(), null, 100, "blue",null, null);
		players.add(player1);
		players.add(player2);
		List<Tile> tiles = GameService.getTileListFromCSV(path,players);
		
		int width = GameService.getWidthCSV(path);
		int height = GameService.getHeightCSV(path);
		Game game = new Game(tiles,players,width,height);
		for(Player player : players) {
			player.setGame(game);
		}
		
		// -- Test Attackable Cases, Movable Cases --

		//Create test units
		Unit[] units = new Unit[]{
			new Commander(player1, 4, 3),
			new Soldier(player1, 11,11),
		};
		
		for(Unit u : units) {
			player1.getUnits().add(u);
			u.setTileSelectionBox();
		}
		
		Unit[] units2 = new Unit[]{
			new Soldier(player2, 1,10),
			new Archer(player2,9,5)
		};
		
		for(Unit u : units2) {
			player2.getUnits().add(u);
			u.setTileSelectionBox();
		}
		units2[0].attack((Building)game.getTileAt(0, 10));
		
		
		assertEquals(196, units2[0].getHp());
		assertEquals(1120, ((Building)game.getTileAt(0, 10)).getHp());
	}

}
