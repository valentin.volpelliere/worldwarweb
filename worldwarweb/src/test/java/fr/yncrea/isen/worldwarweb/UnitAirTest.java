package fr.yncrea.isen.worldwarweb;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import worldwarweb.model.unit.UnitAir;
import worldwarweb.model.unit.UnitWater;
import worldwarweb.model.unit.implementations.BattleShip;
import worldwarweb.model.unit.implementations.Boat;
import worldwarweb.model.unit.implementations.Helicopter;
import worldwarweb.model.unit.implementations.Plane;
import worldwarweb.model.unit.implementations.Soldier;
import worldwarweb.model.unit.implementations.StealthPlane;
import worldwarweb.model.unit.implementations.Submarine;

class UnitAirTest {

	@Test
	final void testGetAttackEfficiency() {
		Helicopter helicopter = new Helicopter();
		Plane plane = new Plane();
		StealthPlane stealthplane = new StealthPlane();
		
		Soldier soldier = new Soldier();
		
		assertEquals(1.25f, helicopter.getAttackEfficiency(soldier));
		assertEquals(1, helicopter.getAttackEfficiency(plane));
		
		assertEquals(1.1f, stealthplane.getAttackEfficiency(soldier));
		assertEquals(0, stealthplane.getAttackEfficiency(plane));
		
		assertEquals(0, plane.getAttackEfficiency(soldier));
		assertEquals(1.5f, plane.getAttackEfficiency(stealthplane));
	}

	@Test
	final void testUnitAir() {
		Helicopter helicopter = new Helicopter();
		Plane plane = new Plane();
		StealthPlane stealthplane = new StealthPlane();
		
		assertTrue(helicopter instanceof UnitAir);
		assertTrue(plane instanceof UnitAir);
		assertTrue(stealthplane  instanceof UnitAir);
	}

	@Test
	final void testUnitAirPlayerStringIntIntIntIntIntIntIntIntInt() {
		Helicopter helicopter = new Helicopter(null,0,0);
		Plane plane = new Plane(null,0,0);
		StealthPlane stealthplane = new StealthPlane(null,0,0);
		
		assertTrue(helicopter instanceof UnitAir);
		assertTrue(plane instanceof UnitAir);
		assertTrue(stealthplane  instanceof UnitAir);
	}

}
